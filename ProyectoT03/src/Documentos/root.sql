Create table LIBRO(
Codigo int  NOT NULL UNIQUE,
Titulo varchar2(255),
Autor int,
A�o int,
Editorial int,
Genero  int,
Idioma  varchar2(60),
Formato varchar2(60),
Resumen varchar2(500),
CONSTRAINT PK_CODIGO_LIBRO PRIMARY KEY
);

Create table EDITORIAL(
Codigo int  NOT NULL UNIQUE,
Nombre  varchar(255),
Direccion varchar2(255),
Telefono  int,
CONSTRAINT PK_CODIGO_EDITORIAL PRIMARY KEY
);

Create table AUTOR(
Codigo int NOT NULL UNIQUE,
Nombre varchar2(60),
apellido_1 varchar2(60),
apellido_2 varchar2(60),
Nacionalidad varchar2(60),
Fecha_nacimiento varchar2(60),
Biografia varchar2(1000),
CONSTRAINT PK_CODIGO_AUTOR PRIMARY KEY
);

Create table GENERO(
Codigo int  NOT NULL UNIQUE,
Tipo varchar2 (20)UNIQUE NOT NULL,
CONSTRAINT PK_CODIGO_GENERO PRIMARY KEY
);

Create table USUARIO(
Codigo int ,
DNI VARCHAR2(9) UNIQUE NOT NULL,
Nombre varchar2(60) NOT NULL,
Apellido_1 varchar2(60) NOT NULL,
Apellido_2 varchar2(60) NOT NULL,
Nickname varchar2(10) UNIQUE NOT NULL,
Email varchar2(30) UNIQUE NOT NULL,
Telefono int, 
FchaNacimiento date NOT NULL,
CONSTRAINT PK_CODIGO_USUARIO PRIMARY KEY (Codigo)
);

Create table ESCRIBE(
CodAutor int NOT NULL  ,
CodLibro int NOT NULL,
CONSTRAINT PK_CODIGO_ESCRIBE PRIMARY KEY(codAutor)
);

ALTER TABLE ESCRIBE
ADD CONSTRAINT PK_CODLIBRO PRIMARY KEY (CodLibro);


Create table PUBLICA(
CodEditorial int,
CodLibro int,
CONSTRAINT PK_CODIGO_PUBLICA PRIMARY KEY (CodEditorial, Codlibro)
);

Create table CLASIFICA(
CodLibro int,
CodGenero int,
CONSTRAINT PK_CODIGO_CLASIFICA PRIMARY KEY (CodLibro, CodGenero)
);

Create table ORDENES(
CodOrden int,
FechaOrden date,
CodUsuario int,
PrecioTotal int,
CodLibro int,
CONSTRAINT PK_CODIGO_ORDENES PRIMARY KEY (CodOrden)
);

Create table FORMADEPAGO(
CodForma int,
PagoDescripcion varchar2(255),
validacion varchar2(2) constraint ch_validacion CHECK (Validacion In('Si','No')),
CONSTRAINT PK_CODIGO_FORMADEPAGO PRIMARY KEY (CodForma)
);

Create table PEDIDO(
CodOrdenPedido int,
CodLibroPedido int,
constraint fk_pedido_orden foreign key (codordenpedido) references ORDEN(Codorden),
constraint fk_pedido_libro foreign key (codlibropedido) references LIBRO(CodLibro)
);

alter table pedido add CONSTRAINT PK_CODIGO_PEDIDO PRIMARY KEY (CodOrden,CodLibro);

Create table REALIZA(
CodOrdenRealiza int,
CodUsuarioRealiza int,
constraint fk_realiza_orden foreign key (CodOrdenRealiza) references ORDEN(Codorden),
constraint fk_realiza_usuario foreign key (codlibropedido) references USUARIO(CodUSUARIO),
CONSTRAINT PK_CODIGO_ORDEN_REALIZA PRIMARY KEY (CodOrden, CodUsuario)
);
alter table realiza add CONSTRAINT PK_CODIGO_USUARIO_REALIZA PRIMARY KEY (CodUsuario);