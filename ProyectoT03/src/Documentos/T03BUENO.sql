/*
CREATE USER oracle
IDENTIFIED BY root
;
GRANT CONNECT, RESOURCE TO PRUEBAT03; 
*/

Create table LIBRO(
Codigo int  NOT NULL ,
ISBN int NOT NULL,
Titulo varchar2(255),
Autor int,
A�o int,
Editorial int,
Genero  int,
Idioma  varchar2(60),
Formato varchar2(60),
Resumen varchar2(500),
CONSTRAINT PK_CODIGO_LIBRO PRIMARY KEY (Codigo)
);
CREATE TABLE PUBLICACION_USUARIO(
CodigoPublicacion INT,
NombreArchivo VARCHAR2(60),
FechaPublicacion DATE, 
UsuarioAutor int,
Genero VARCHAR2(30),
Idioma varchar2(20),
Formato varchar2(10),
CONSTRAINT PK_PUBLICACIONES PRIMARY KEY (CodigoPublicacion)
);

Create table EDITORIAL(
Codigo int  NOT NULL,
Nombre  varchar(255) UNIQUE,
Direccion varchar2(255) UNIQUE,
Telefono  int UNIQUE,
CONSTRAINT PK_CODIGO_EDITORIAL PRIMARY KEY (Codigo)
);

Create table AUTOR(
Codigo int NOT NULL ,
Nombre varchar2(60),
apellido_1 varchar2(60),
apellido_2 varchar2(60),
Nacionalidad varchar2(60),
Fecha_nacimiento varchar2(60),
Biografia varchar2(1000),
CONSTRAINT PK_CODIGO_AUTOR PRIMARY KEY (Codigo)
);

Create table GENERO(
Codigo int  NOT NULL,
Tipo varchar2 (20)UNIQUE NOT NULL,
CONSTRAINT PK_CODIGO_GENERO PRIMARY KEY (Codigo)
);

Create table USUARIO(
Codigo int not null ,
DNI VARCHAR2(9) UNIQUE NOT NULL,
Nombre varchar2(60) NOT NULL,
Apellido_1 varchar2(60) NOT NULL,
Apellido_2 varchar2(60) NOT NULL,
Nickname varchar2(10) UNIQUE NOT NULL,
Pass  varchar2(20) NOT NULL, 
Email varchar2(30) UNIQUE NOT NULL,
Telefono int, 
FchaNacimiento date NOT NULL,
Biblioteca int not null UNIQUE,
Preferencia varchar2(200),
CONSTRAINT PK_CODIGO_USUARIO PRIMARY KEY (Codigo)
);

Create table ESCRIBE(
CodAutor int NOT NULL,
CodLibro int NOT NULL
/*CONSTRAINT PK_CODIGO_ESCRIBE PRIMARY KEY(codAutor)*/
);

/*ALTER TABLE ESCRIBE
ADD CONSTRAINT PK_CODLIBRO PRIMARY KEY (CodLibro);*/


Create table PUBLICA(
CodEditorial int,
CodLibro int,
CONSTRAINT PK_CODIGO_PUBLICA PRIMARY KEY (CodEditorial, Codlibro)
);

Create table CLASIFICA(
CodLibro int,
CodGenero int,
CONSTRAINT PK_CODIGO_CLASIFICA PRIMARY KEY (CodLibro, CodGenero)
);

Create table ORDENES(
CodOrden int,
FechaOrden date,
CodUsuario int,
PrecioTotal int,
CodLibro int,
CONSTRAINT PK_CODIGO_ORDENES PRIMARY KEY (CodOrden)
);



CREATE TABLE BIBLIOTECA (
CODIGOBIBLIOTECA INT NOT NULL,
CODIGOLIBRO INT,
USUARIO INT
);

ALTER TABLE BIBLIOTECA
ADD CONSTRAINT FK_USUARIO_BIBLIOTECA FOREIGN KEY (USUARIO) REFERENCES USUARIO (CODIGO);

ALTER TABLE BIBLIOTECA
ADD CONSTRAINT FK_BIBLIOTECA FOREIGN KEY (CODIGOLIBRO) REFERENCES LIBRO (CODIGO);

ALTER TABLE BIBLIOTECA
ADD CONSTRAINT FK_BIBLIOUSU_BIBLIO FOREIGN KEY (CODIGOBIBLIOTECA) REFERENCES USUARIO(BIBLIOTECA);

ALTER TABLE PUBLICA
ADD CONSTRAINT FK_EDITA FOREIGN KEY (CODEDITORIAL)REFERENCES EDITORIAL (CODIGO);

ALTER TABLE PUBLICA 
ADD CONSTRAINT FK_PUBLICA FOREIGN KEY (CODLIBRO) REFERENCES LIBRO (CODIGO);

ALTER TABLE ESCRIBE
ADD CONSTRAINT FK_AUTOR_ESCRIBE FOREIGN KEY (CODAUTOR) REFERENCES AUTOR (CODIGO);

ALTER TABLE ESCRIBE
ADD CONSTRAINT FK_LIBRO_ESCRIBE FOREIGN KEY (CODLIBRO) REFERENCES LIBRO (CODIGO);

ALTER TABLE CLASIFICA
ADD CONSTRAINT FK_CLASIFICA_GENEROS FOREIGN KEY (CODGENERO) REFERENCES GENERO (CODIGO);

ALTER TABLE CLASIFICA
ADD CONSTRAINT FK_CLASIFICA_LIBRO FOREIGN KEY (CODGENERO) REFERENCES LIBRO (CODIGO);

ALTER TABLE PUBLICACION_USUARIO
ADD CONSTRAINT FK_USUARIO_PUBLICA FOREIGN KEY (CodigoPublicacion) references USUARIO (CODIGO);

ALTER TABLE ORDENES
ADD CONSTRAINT FK_ORDEN_LIBRO FOREIGN KEY (CODLIBRO) REFERENCES LIBRO (CODIGO);

ALTER TABLE ORDENES 
ADD CONSTRAINT FK_ORDEN_USUARIO  FOREIGN KEY (CODUSUARIO) REFERENCES USUARIO (CODIGO);



/*tuplas
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES ('1', 'drama');
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES ('2', 'ciencia ficcion');

INSERT INTO "AUTOR" (CODIGO, NOMBRE, APELLIDO_1, APELLIDO_2, NACIONALIDAD, FECHA_NACIMIENTO, BIOGRAFIA) VALUES ('1', 'javi', 'soto', 'ordo�ez', 'catalana', '11/2/1991', 'mota gente');
INSERT INTO "AUTOR" (CODIGO, NOMBRE, APELLIDO_1, APELLIDO_2, NACIONALIDAD, FECHA_NACIMIENTO, BIOGRAFIA) VALUES ('2', 'nuria', 'sanahuja', 'selva', 'catalana', '29/3/1991', 'toca el piano');

INSERT INTO "EDITORIAL" (CODIGO, NOMBRE, DIRECCION, TELEFONO) VALUES ('1', 'cruilla', 'barcelona', '877825365');
INSERT INTO "EDITORIAL" (CODIGO, NOMBRE, DIRECCION, TELEFONO) VALUES ('2', 'vapor', 'tarragona', '89665231');

INSERT INTO "LIBRO" (CODIGO, TITULO, AUTOR, "A�O", EDITORIAL, GENERO, IDIOMA, FORMATO, RESUMEN) VALUES ('1', 'Jarry potar', '1', '1998', '1', '1', 'ingish', 'pdf', 'jarry es majo');
INSERT INTO "LIBRO" (CODIGO, TITULO, AUTOR, "A�O", EDITORIAL, GENERO, IDIOMA, FORMATO, RESUMEN) VALUES ('2', 'se�or de los p...', '2', '1946', '2', '2', 'chekoslovako', 'pdf', 'otro majo');

INSERT INTO "USUARIO" (CODIGO, DNI, NOMBRE, APELLIDO_1, APELLIDO_2, NICKNAME, EMAIL, TELEFONO, FCHANACIMIENTO, BIBLIOTECA) VALUES ('1', '987', 'bob', 'esponja', 'manetas', 'bob1', 'bob1@..', '987', TO_DATE('1989-09-18 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1');
INSERT INTO "USUARIO" (CODIGO, DNI, NOMBRE, APELLIDO_1, APELLIDO_2, NICKNAME, EMAIL, TELEFONO, FCHANACIMIENTO, BIBLIOTECA) VALUES ('2', '78', 'peter', 'griffin', 'parker', 'parkone', 'parker@...', '235', TO_DATE('1654-09-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2';

INSERT INTO "BIBLIOTECA" (CODIGOBIBLIOTECA, CODIGOLIBRO, USUARIO) VALUES ('1', '1', '1');
INSERT INTO "BIBLIOTECA" (CODIGOBIBLIOTECA, CODIGOLIBRO, USUARIO) VALUES ('2', '2', '2');

INSERT INTO "ESCRIBE" (CODAUTOR, CODLIBRO) VALUES ('1', '1');
INSERT INTO "ESCRIBE" (CODAUTOR, CODLIBRO) VALUES ('2', '2');

INSERT INTO "PUBLICA" (CODEDITORIAL, CODLIBRO) VALUES ('1', '1');
INSERT INTO "PUBLICA" (CODEDITORIAL, CODLIBRO) VALUES ('2', '2');

INSERT INTO "CLASIFICA" (CODLIBRO, CODGENERO) VALUES ('1', '1');
INSERT INTO "CLASIFICA" (CODLIBRO, CODGENERO) VALUES ('2', '2');

INSERT INTO "ORDENES" (CODORDEN, FECHAORDEN, CODUSUARIO, PRECIOTOTAL, CODLIBRO) VALUES ('1', TO_DATE('2000-08-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', '98', '1');
INSERT INTO "ORDENES" (CODORDEN, FECHAORDEN, CODUSUARIO, PRECIOTOTAL, CODLIBRO) VALUES ('2', TO_DATE('2023-08-09 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', '89', '2');

INSERT INTO "PUBLICACION_USUARIO" (CODIGOPUBLICACION, NOMBREARCHIVO, FECHAPUBLICACION, USUARIOAUTOR, GENERO, IDIOMA, FORMATO) VALUES ('1', 'ola', TO_DATE('2000-09-08 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '1', 'nada', 'chekoslovako', 'pdf');
INSERT INTO "PUBLICACION_USUARIO" (CODIGOPUBLICACION, NOMBREARCHIVO, FECHAPUBLICACION, USUARIOAUTOR, GENERO, IDIOMA, FORMATO) VALUES ('2', 'adeu', TO_DATE('1978-09-02 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '2', 'nada', 'catala', 'pergamino');
*/



Create Sequence seq_Usuario
Minvalue 1
Start with 100
Increment by 1
;


Create Sequence seq_Autor
Minvalue 1
Start with 100
Increment by 1
;


Create Sequence seq_Libro
Minvalue 1
Start with 100
Increment by 1
;

Create Sequence seq_Biblioteca
Minvalue 1
Start with 100
Increment by 1
;


Create Sequence seq_Editorial
Minvalue 1
Start with 100
Increment by 1
;
Create Sequence seq_Genero
Minvalue 1
Start with 100
Increment by 1
;
Create Sequence seq_Ordenes
Minvalue 1
Start with 100
Increment by 1
;

Create Sequence seq_Publicacion_Usuario
Minvalue 1
Start with 100
Increment by 1
;

/*tuplas
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES (seq_genero.nextval, 'Drama');
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES (seq_genero.nextval, 'Ciencia Ficci�n');
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES (seq_genero.nextval, 'Fantas�a');
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES (seq_genero.nextval, 'Humor');
INSERT INTO "GENERO" (CODIGO, TIPO) VALUES (seq_genero.nextval, 'Infantil');

INSERT INTO "AUTOR" (CODIGO, NOMBRE, APELLIDO_1, APELLIDO_2, NACIONALIDAD, FECHA_NACIMIENTO, BIOGRAFIA) VALUES (seq_Autor.nextval, 'George Raymond', 'Richard', 'Martin', 'USA', '20/9/1948', 'conocido como George R. R. Martin y en ocasiones por sus seguidores como GRRM, es un escritor y guionista estadounidense de literatura fant�stica, ciencia ficci�n y terror');
INSERT INTO "AUTOR" (CODIGO, NOMBRE, APELLIDO_1, APELLIDO_2, NACIONALIDAD, FECHA_NACIMIENTO, BIOGRAFIA) VALUES (seq_Autor.nextval, 'John Ronald', 'Reuel', 'Tolkien', 'ENG', '3/1/1892', 'John Ronald Reuel Tolkien, CBE, a menudo citado como J. R. R. Tolkien o JRRT, fue un escritor, poeta, fil�logo, ling�ista y profesor universitario brit�nico, conocido principalmente por ser el autor de las novelas "The Lord of the Rings"');
INSERT INTO "AUTOR" (CODIGO, NOMBRE, APELLIDO_1, APELLIDO_2, NACIONALIDAD, FECHA_NACIMIENTO, BIOGRAFIA) VALUES (seq_Autor.nextval, 'Joanne', 'K.', 'Rowling', 'ENG', '31/7/1965', 'es una escritora y productora de cine brit�nica, principalmente conocida por ser la creadora de la serie de libros Harry Potter.');


INSERT INTO "EDITORIAL" (CODIGO, NOMBRE, DIRECCION, TELEFONO) VALUES (seq_Editorial.nextval, 'Anagrama', 'c/ bayern N 17 TARRAGONA', '650456932');
INSERT INTO "EDITORIAL" (CODIGO, NOMBRE, DIRECCION, TELEFONO) VALUES (seq_Editorial.nextval, 'Planeta', 'c/ Bisbe Domenech N 20 TARRAGONA', '620154469');
*/
