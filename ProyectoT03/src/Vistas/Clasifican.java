package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Clasifican extends JFrame {
	private DefaultListModel<String> mLista = new DefaultListModel<>();
	private JPanel contentPane;
	private JList list = new JList(mLista);
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	int i=0;
	private static Connection Conexion=null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clasifican frame = new Clasifican();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Clasifican() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 508, 646);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGenero = new JLabel("Elegir Genero");
		lblGenero.setForeground(Color.WHITE);
		lblGenero.setBounds(29, 66, 105, 17);
		lblGenero.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblGenero);
		
		JLabel lblGeneros = new JLabel("Libros");
		lblGeneros.setForeground(Color.WHITE);
		lblGeneros.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblGeneros.setBounds(29, 100, 77, 22);
		contentPane.add(lblGeneros);
		
		list.setBounds(29, 133, 285, 350);
		contentPane.add(list);
		
		
		
		final JComboBox comboBox_1 = new JComboBox();
		
		comboBox_1.setBounds(144, 64, 170, 25);
		contentPane.add(comboBox_1);
		
		SQLAutor db = new SQLAutor();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT TIPO FROM GENERO ORDER BY TIPO ASC";
			
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
			
            st.executeQuery(QueryAutor);
            
            ResultSet rs = st.executeQuery(QueryAutor);
           
            while(rs.next()){
            	
            	comboBox_1.addItem(rs.getObject("TIPO"));
            	
            }
            db.closeConnection();
            
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		JButton btnNewButton = new JButton("Atr\u00E1s");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(29, 519, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Clasifican.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 492, 608);
		contentPane.add(lblNewLabel);
		
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				mLista.removeAllElements();
				try {
					String QA="select codigo from genero where tipo="+"'"+comboBox_1.getSelectedItem()+"'";
		            
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            Statement rt= Conexion.createStatement();
		            ResultSet rs;
		            ResultSet rq;
		            
		            rq = rt.executeQuery(QA);
				
		            
		            
		            while (rq.next()) {
		             i=rq.getInt("CODIGO");
		             
		            }
		            
		            String QueryAutor = "select titulo from libro where GENERO="+i;
		            rs = st.executeQuery(QueryAutor);
		            while (rs.next()){
		            	mLista.addElement(rs.getString("TITULO"));
		            }
				

				} catch (SQLException  ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			}
		});
		
	}

}
