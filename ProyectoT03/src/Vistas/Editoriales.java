package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Editoriales extends JFrame {

	private JPanel contentPane;
	private JPanel contentPane_1;
	private JTextField textField_1;
	private JLabel lblDireccion;
	private JTextField textField_2;
	private JLabel lblNewLabel_1;
	private JTextField textField_3;
	private JButton btnInsertar;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Editoriales frame = new Editoriales();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Editoriales() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Ingresar Editorial");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 436, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		
		
		
		
		
		
		
		
		
		
		
		contentPane_1 = new JPanel();
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(40, 64, 71, 19);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNombre);
		
		textField_1 = new JTextField();
		textField_1.setBounds(200, 61, 136, 25);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane_1.add(textField_1);
		textField_1.setColumns(10);
		
		lblDireccion = new JLabel("Direcci\u00F3n");
		lblDireccion.setForeground(Color.WHITE);
		lblDireccion.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDireccion.setBounds(40, 124, 71, 20);
		contentPane_1.add(lblDireccion);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_2.setBounds(200, 122, 136, 25);
		contentPane_1.add(textField_2);
		textField_2.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Tel\u00E9fono");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 192, 71, 22);
		contentPane_1.add(lblNewLabel_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(200, 189, 136, 25);
		contentPane_1.add(textField_3);
		textField_3.setColumns(10);
		
		btnInsertar = new JButton("Insertar");
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLEditoriales db = new SQLEditoriales();
		        try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       db.insertDataEditorial(textField_1.getText(), textField_2.getText(), textField_3.getText());
		               
		                
		               
		        db.closeConnection();
			}
		});
		btnInsertar.setBounds(247, 259, 89, 23);
		contentPane_1.add(btnInsertar);
		
		JButton btnAtras = new JButton("Atras");
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtras.setBounds(40, 259, 89, 23);
		contentPane_1.add(btnAtras);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Editoriales.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 420, 313);
		contentPane_1.add(lblNewLabel);
		
		
	}

}
