package Vistas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQLAutor {
	private static Connection Conexion = null;
	
	//M閠odo para iniciar sesi髇
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi贸n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi贸n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi贸n con el servidor");
	        }
	    }
	     

		//M閠odo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
		/*public void createDB(String name) {
	        try {
	            String Query = "CREATE DATABASE " + name;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            SQLConnection("root", "", name);
	            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }*/
		
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		/* public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		*/
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertDataAutor(String NOMBRE, String APELLIDO1, String APELLIDO2, String NACIONALIDAD, String FECHA_NACIMIENTO, String BIOGRAFIA) {
		        try {
		            String QueryAutor = "INSERT INTO AUTOR VALUES"
		            		+"(seq_Autor.nextval,"
		            		//+ "'"+ CODIGO + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ APELLIDO1 + "',"
		            		+ "'"+ APELLIDO2 + "',"
		            		+ "'"+ NACIONALIDAD + "',"
		            		+ "'"+ FECHA_NACIMIENTO + "',"
		            		+ "'"+ BIOGRAFIA + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		
			
		
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(Object CODIGO) {
		        try {
		            String QueryAutor = "DELETE FROM AUTOR WHERE CODIGO = '" + CODIGO + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void alterRecord(Object CODIGO, String NOMBRE, String APELLIDO1, String APELLIDO2, String NACIONALIDAD, String FECHA_NACIMIENTO, String BIOGRAFIA) {
		        try {
		            String QueryAutor = "UPDATE AUTOR set NOMBRE='"+NOMBRE +"',APELLIDO1='"+APELLIDO1+"',APELLIDO2='"+APELLIDO2+"',NACIONALIDAD='"+NACIONALIDAD+"',FECHA_NACIMIENTO='"+FECHA_NACIMIENTO+"',BIOGRAFIA'"+BIOGRAFIA    + "'  WHERE ID = '" + CODIGO + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);
		            

		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 
		 public void mostrarTupla( Object object, String NOMBRE, String APELLIDO1, String APELLIDO2, String NACIONALIDAD, String FECHA_NACIMIENTO, String BIOGRAFIA) {
		        try {
		            String QueryAutor = "UPDATE AUTOR set NOMBRE='"+NOMBRE +"',APELLIDO_1='"+APELLIDO1+"',APELLIDO_2='"+APELLIDO2+"',NACIONALIDAD='"+NACIONALIDAD+"',FECHA_NACIMIENTO='"+FECHA_NACIMIENTO+"',BIOGRAFIA='"+BIOGRAFIA    + "'  WHERE CODIGO = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);
		            

		            

		      } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error EDITANDO el registro especificado");
		        }
		  
		 }
		 
		 	//No se usa
			/* public void slectID(){
				 try {
			            String Query = "SELECT ID FROM AUTOR";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			            
			            while(resultSet.next()){
			            	resultSet.getString("ID");
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici贸n de datos");
			        }
			        
			        
			    }*/
			 
			//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
			 public void getValues() {
			        try {
			            String QueryAutor = "SELECT * FROM AUTOR";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(QueryAutor);

			            while (resultSet.next()) {
			                System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
			                        + "Nombre: " + resultSet.getString("NOMBRE") + " " 
									+ resultSet.getString("APELLIDO1") + " "+resultSet.getString("APELLIDO2")
			                        + "Nacionalidad: " + resultSet.getString("NACIONALIDAD") + " "
			                        + "Fecha_Nacimiento: " + resultSet.getString("FECHA_NACIMIENTO")+" "+
			                        "Biografia: "+ resultSet.getString("BIOGRAFIA"));
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici贸n de datos");
			        }
			    }
			 
}

