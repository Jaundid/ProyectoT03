package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;


public class GeneroEditar extends JFrame {

	private String dbname="";
	private String user="oracle";
	private String pass="root";
	private static Connection Conexion=null;
	private JPanel contentPane;
	private JTextField textField_Tipo;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GeneroEditar frame = new GeneroEditar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GeneroEditar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("GeneroEditar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setForeground(Color.WHITE);
		lblCodigo.setBounds(40, 73, 64, 19);
		lblCodigo.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblCodigo);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setForeground(Color.WHITE);
		lblTipo.setBounds(40, 122, 64, 19);
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblTipo);
		
	
		
		JButton btnAtras = new JButton("Atras");
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		
		final JComboBox<Object> comboBox = new JComboBox();
		comboBox.setBounds(146, 72, 140, 20);
		SQLGenero db = new SQLGenero();				
		contentPane.add(comboBox);		
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT CODIGO FROM Genero";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeUpdate(QueryAutor);
            ResultSet rs = st.executeQuery(QueryAutor);
            while(rs.next()){
            	
            	comboBox.addItem(rs.getObject("CODIGO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		btnAtras.setBounds(40, 243, 89, 23);
		contentPane.add(btnAtras);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLLibros db = new SQLLibros();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.deleteRecord(comboBox.getSelectedItem());
		
				}
        });
		
		btnBorrar.setBounds(146, 243, 89, 23);
		contentPane.add(btnBorrar);
		
	
		textField_Tipo = new JTextField();
		textField_Tipo.setBounds(146, 121, 140, 20);
		contentPane.add(textField_Tipo);
		textField_Tipo.setColumns(10);
	
		
		JButton btnEditar = new JButton("Editar");						
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnEditar.setBounds(245, 243, 89, 23);
		contentPane.add(btnEditar);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(GeneroEditar.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 364, 313);
		contentPane.add(lblNewLabel);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLGenero db = new SQLGenero();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.mostrarTupla(comboBox.getSelectedItem(), textField_Tipo.getText());
			
			}
		});
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
		            String QueryGenero = "SELECT * FROM TIPO WHERE CODIGO = "+comboBox.getSelectedItem();
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryGenero);
				
				
		            while (resultSet.next()) {
		            	
		            	
		            	
		            	textField_Tipo.setText(resultSet.getString("TIPO"));
		            	
		            }
				
				

				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				
			
		}
			
		});
		
		
	}
}

