package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import java.awt.Color;
public class Autores extends JFrame {

	private JPanel contentPane;
	private JPanel contentPane_1;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JTextField textFieldFNac;
	private JTextField textFieldBiografia;
	private JTextField textFieldApellido2;
	private JTextField textFieldNacionalidad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Autores frame = new Autores();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Autores() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Ingresar autor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 534);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		//SpringLayout sl_contentPane = new SpringLayout();
		//contentPane.setLayout(sl_contentPane);
		
		
		contentPane_1 = new JPanel();
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(40, 110, 83, 23);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setForeground(Color.WHITE);
		lblApellido.setBounds(40, 150, 83, 14);
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblApellido);
		
		JLabel Apellido2 = new JLabel("Apellido2");
		Apellido2.setForeground(Color.WHITE);
		Apellido2.setFont(new Font("Tahoma", Font.BOLD, 15));
		Apellido2.setBounds(40, 189, 83, 19);
		contentPane_1.add(Apellido2);
		
		JLabel lblFechanacimiento = new JLabel("Fecha_nacimiento");
		lblFechanacimiento.setForeground(Color.WHITE);
		lblFechanacimiento.setBounds(40, 275, 147, 23);
		lblFechanacimiento.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblFechanacimiento);
		
		JLabel lblBibliografa = new JLabel("Bibliograf\u00EDa");
		lblBibliografa.setForeground(Color.WHITE);
		lblBibliografa.setBounds(40, 327, 124, 23);
		lblBibliografa.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblBibliografa);
		
		final JButton btnNewButton = new JButton("Atras");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(40, 417, 89, 23);
		contentPane_1.add(btnNewButton);
		
		JButton btnInsertar = new JButton("Insertar");
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLAutor db = new SQLAutor();
		        try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       db.insertDataAutor(textFieldNombre.getText(), textFieldApellido.getText(), textFieldApellido2.getText(), textFieldNacionalidad.getText(), textFieldFNac.getText(), textFieldBiografia.getText());
		               
		                
		               
		        db.closeConnection();
		      
				
			}

			
		});
		
		
		btnInsertar.setBounds(314, 417, 89, 23);
		contentPane_1.add(btnInsertar);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(317, 113, 86, 20);
		textFieldNombre.setColumns(10);
		contentPane_1.add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setBounds(317, 149, 86, 20);
		textFieldApellido.setColumns(10);
		contentPane_1.add(textFieldApellido);
		
		textFieldApellido2 = new JTextField();
		textFieldApellido2.setColumns(10);
		textFieldApellido2.setBounds(317, 190, 86, 20);
		contentPane_1.add(textFieldApellido2);
		
		textFieldNacionalidad = new JTextField();
		textFieldNacionalidad.setBounds(317, 232, 86, 20);
		contentPane_1.add(textFieldNacionalidad);
		textFieldNacionalidad.setColumns(10);
		
		textFieldFNac = new JTextField();
		textFieldFNac.setBounds(317, 278, 86, 20);
		textFieldFNac.setColumns(10);
		contentPane_1.add(textFieldFNac);
		
		textFieldBiografia = new JTextField();
		textFieldBiografia.setBounds(317, 330, 86, 20);
		textFieldBiografia.setColumns(10);
		contentPane_1.add(textFieldBiografia);
		
		JLabel lblNewLabel_1 = new JLabel("Nacionalidad");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 231, 108, 19);
		contentPane_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Autores.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 534, 496);
		contentPane_1.add(lblNewLabel);
		
		
	}
}

