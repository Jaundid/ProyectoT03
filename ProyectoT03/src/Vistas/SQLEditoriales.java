package Vistas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class SQLEditoriales {

private static Connection Conexion = null;
	
	//M�todo para iniciar sesi�n
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        }
	    }
	     

		//M�todo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
		/*public void createDB(String name) {
	        try {
	            String Query = "CREATE DATABASE " + name;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            SQLConnection("root", "", name);
	            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }*/
		
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		/* public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		*/
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertDataEditorial(String NOMBRE, String DIRECCION, String TELEFONO) {
		        try {
		            String QueryEditorial = "INSERT INTO Editorial VALUES("
		            		+"seq_Editorial.nextval"+","
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ DIRECCION + "',"
		            		+ "'"+ TELEFONO + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryEditorial);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValuesEditorial() {
		        try {
		            String QueryEditorial = "SELECT * FROM EDITORIAL";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryEditorial);

		            while (resultSet.next()) {
		                System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
		                        + "Nombre: " + resultSet.getString("NOMBRE") + " " 
		                        + "Direccion: " + resultSet.getString("DIRECCION") + " "
		                        + "Telefono " + resultSet.getString("TELEFONO"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        }
		    }	
		
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecordEditorial(Object object) {
		        try {
		            String QueryEditorial = "DELETE FROM EDITORIAL WHERE codigo = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryEditorial);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void alterRecordEditorial(String CODIGO, String NOMBRE, String APELLIDO1, String APELLIDO2, String NACIONALIDAD, String FECHA_NACIMIENTO, String BIOGRAFIA) {
		        try {
		            String QueryEditorial = "UPDATE Editorial set NOMBRE='"+NOMBRE +"',APELLIDO1='"+APELLIDO1+"',APELLIDO2='"+APELLIDO2+"',NACIONALIDAD='"+NACIONALIDAD+"',FECHA_NACIMIENTO='"+FECHA_NACIMIENTO+"',BIOGRAFIA='"+BIOGRAFIA    + "'  WHERE codigo = '" + CODIGO + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryEditorial);
		            

		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 
		 public void mostrarTupla( Object object, String NOMBRE, String DIRECCION, String TELEFONO) {
		        try {
		            String QueryEditorial = "UPDATE EDITORIAL set NOMBRE='"+NOMBRE +"',DIRECCION='"+DIRECCION+"',TELEFONO='"+TELEFONO+"'  WHERE CODIGO = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryEditorial);
		            

		            

		      } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error EDITANDO el registro especificado");
		        }
		  
		 }
}
