package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Publica extends JFrame {

	private JPanel contentPane;
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;
	int i=0;
	private DefaultListModel<String> mLista = new DefaultListModel<>();
	private JList list = new JList(mLista);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Publica frame = new Publica();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Publica() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Editorial Publica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 357, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JComboBox comboBoxEditorial = new JComboBox();
		
		comboBoxEditorial.setBounds(111, 28, 99, 20);
		contentPane.add(comboBoxEditorial);
		
		JLabel lblEditorial = new JLabel("Editorial");
		lblEditorial.setForeground(Color.WHITE);
		lblEditorial.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEditorial.setBounds(29, 28, 72, 17);
		contentPane.add(lblEditorial);
		
		JLabel lblLibros = new JLabel("Libros");
		lblLibros.setForeground(Color.WHITE);
		lblLibros.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblLibros.setBounds(29, 76, 72, 20);
		contentPane.add(lblLibros);
		
		JButton btnNewButton = new JButton("Atr\u00E1s");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnNewButton.setBounds(10, 375, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnBorrar.setBounds(111, 375, 89, 23);
		contentPane.add(btnBorrar);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnEditar.setBounds(218, 375, 89, 23);
		contentPane.add(btnEditar);
		SQLAutor db = new SQLAutor();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT NOMBRE FROM EDITORIAL ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryAutor);
            ResultSet rs = st.executeQuery(QueryAutor);
            while(rs.next()){
            	
            	comboBoxEditorial.addItem(rs.getObject("NOMBRE"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		final JComboBox comboBoxLibro = new JComboBox();
		list.setBounds(111, 80, 196, 250);
		contentPane.add(list);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Publica.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 341, 456);
		contentPane.add(lblNewLabel);
		comboBoxEditorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					mLista.removeAllElements();
		            String QueryAutor = "select codigo from Editorial where nombre="+"'"+comboBoxEditorial.getSelectedItem()+"'"; //aqui pillar� el codigo seg�n el nombre seleccionado del combobox
		            
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            Statement rt=Conexion.createStatement();
		            
		            ResultSet rA= st.executeQuery(QueryAutor);
		            
				
				
		            while (rA.next()) {
		            i=rA.getInt("CODIGO"); //aqu� lo almacenar� en la variable
		             //list.setModel(mLista);
		            
		            }
		            String QA="select titulo from libro where Editorial="+i; //seleccionara el titulo segun el codigo del autor
		            ResultSet RL= rt.executeQuery(QA);

		            while(RL.next()){
		            	mLista.addElement(RL.getString("TITULO")); //mostrara el titulo, si a�ades un libro a un autor, teniendo 3 autores, y estos 2 restantes no tienen libros, ver�s que solo saldr� el de 1 autor
		            }
		            
				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        }
			}
		});
		
		
	}
}
