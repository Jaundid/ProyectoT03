package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class EditorialesEditar extends JFrame {
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;
	private JPanel contentPane;
	private JPanel contentPane_1;
	private JTextField textField_1;
	private JLabel lblDireccion;
	private JTextField textField_2;
	private JLabel lblNewLabel_1;
	private JTextField textField_3;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditorialesEditar frame = new EditorialesEditar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditorialesEditar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Editoriales Editar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		
		
		
		
		
		
		
		
		
		
		
		contentPane_1 = new JPanel();
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		
		
		
		
		JLabel lblNewLabel = new JLabel("Codigo");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(40, 75, 71, 19);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(40, 115, 71, 19);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNombre);
		
		textField_1 = new JTextField();
		textField_1.setBounds(200, 110, 136, 25);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		contentPane_1.add(textField_1);
		textField_1.setColumns(10);
		
		lblDireccion = new JLabel("Direcci\u00F3n");
		lblDireccion.setForeground(Color.WHITE);
		lblDireccion.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblDireccion.setBounds(40, 155, 71, 20);
		contentPane_1.add(lblDireccion);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_2.setBounds(200, 150, 136, 25);
		contentPane_1.add(textField_2);
		textField_2.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Tel\u00E9fono");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 195, 71, 20);
		contentPane_1.add(lblNewLabel_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(200, 190, 136, 25);
		contentPane_1.add(textField_3);
		textField_3.setColumns(10);
		
		btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnEditar.setBounds(299, 259, 89, 23);
		contentPane_1.add(btnEditar);
		
		JButton btnAtras = new JButton("Atras");
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtras.setBounds(22, 259, 89, 23);
		contentPane_1.add(btnAtras);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
	
		btnBorrar.setBounds(200, 259, 89, 23);
		contentPane_1.add(btnBorrar);
		
		
		
		final JComboBox<Object> comboBox = new JComboBox();
		comboBox.setBounds(200, 75, 136, 23);
		contentPane_1.add(comboBox);
		
		lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(EditorialesEditar.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_2.setBounds(0, 0, 435, 313);
		contentPane_1.add(lblNewLabel_2);
		SQLEditoriales db = new SQLEditoriales();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryEditoriales = "SELECT CODIGO FROM EDITORIAL ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryEditoriales);
            ResultSet rs = st.executeQuery(QueryEditoriales);
            while(rs.next()){
            	
            	comboBox.addItem(rs.getObject("CODIGO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQLEditoriales db = new SQLEditoriales();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.deleteRecordEditorial(comboBox.getSelectedItem());
		
				
			}
		});
		
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLEditoriales db = new SQLEditoriales();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.mostrarTupla(comboBox.getSelectedItem(), textField_1.getText(),textField_2.getText(), textField_3.getText());
			
			}
		});
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
		            String QueryEditorial = "SELECT * FROM EDITORIAL WHERE CODIGO = "+comboBox.getSelectedItem();
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryEditorial);
				
				
		            while (resultSet.next()) {
		            	
		            	
		            	
		            	textField_1.setText(resultSet.getString("NOMBRE"));
		            	textField_2.setText(resultSet.getString("DIRECCION"));
		            	textField_3.setText(resultSet.getString("TELEFONO"));
		               /* System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
		                        + "Nombre: " + resultSet.getString("NOMBRE") + " " 
								+ resultSet.getString("APELLIDO1") + " "+resultSet.getString("APELLIDO2")
		                        + "Nacionalidad: " + resultSet.getString("NACIONALIDAD") + " "
		                        + "Fecha_Nacimiento: " + resultSet.getString("FECHA_NACIMIENTO")+" "+
		                        "Biografia: "+ resultSet.getString("BIOGRAFIA"));*/
		            }
				
				

				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				
			
		}
			
		});
		
		
			}
		
	}

