package Vistas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class SQLpublicaci�nUsuario {
private static Connection Conexion = null;
	
	//M�todo para iniciar sesi�n
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        }
	    }
	     

		//M�todo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
		/*public void createDB(String name) {
	        try {
	            String Query = "CREATE DATABASE " + name;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            SQLConnection("root", "", name);
	            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }*/
		
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		/* public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		*/
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertDataPubliUsuario(String NOMBREARCHIVO, Object USUARIOAUTOR, Object GENERO, String IDIOMA, String FORMATO) {
		        try {
		            String QueryPublicaUsuario = "INSERT INTO PUBLICACION_USUARIO VALUES("
		            		+"seq_Publicacion_Usuario.nextval"+","
		            		+ "'"+ NOMBREARCHIVO + "',"
		            		+ "SYSDATE"+","
		            		+ "'"+ USUARIOAUTOR + "',"
		            		+ "'"+ GENERO + "',"
		            		+ "'"+ IDIOMA + "',"
		            		+ "'"+ FORMATO + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryPublicaUsuario);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValuesPubliUsuario() {
		        try {
		            String QueryPubliUsuario = "SELECT * FROM PUBLICACION_USUARIO";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryPubliUsuario);

		            while (resultSet.next()) {
		                System.out.println("Codigo: " + resultSet.getString("CODIGOPUBLICACION") + " "
		                        + "Nombre: " + resultSet.getString("NOMBREARCHIVO") + " " 
								+ "FECHA PUBLI: " + resultSet.getString("FECHAPUBLICACION") + " " 
								+"Usuario Autor:" +resultSet.getString("USUARIOAUTOR")
		                        + "Genero: " + resultSet.getString("GENERO") + " "
		                        + "Idioma: " + resultSet.getString("IDIOMA")+" "
		                        + "Formato: "+ resultSet.getString("FORMATO"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        }
		    }	
		
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecordPubiUsuario(Object object) {
		        try {
		            String QueryPUsuario = "DELETE FROM PUBLICACION_USUARIO WHERE CODIGOPUBLICACION ="+object;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryPUsuario);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void alterRecordPubli(Object object,String NOMBREARCHIVOS, Object USUARIOAUTOR, Object GENERO, String IDIOMA, String FORMATO) {
		        try {
		            String QueryPUsuario = "UPDATE PUBLICACION_USUARIO set "
		            		+"NOMBREARCHIVO="+"'"+NOMBREARCHIVOS+"',"
		            		+"USUARIOAUTOR='"+USUARIOAUTOR+"',"
		            		+"GENERO="+"'"+GENERO+"',"
		            		+"IDIOMA="+"'"+IDIOMA+"',"
		            		+"FORMATO="+"'"+FORMATO+"'"+" WHERE CODIGOPUBLICACION ="+object;
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryPUsuario);
		            

		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 
		 public void mostarTupla(String CODIGO) {
		      //  try {
		            //String QueryAutor = "UPDATE Autores set NOMBRE='"+NOMBRE +"',APELLIDO1='"+APELLIDO1+"',APELLIDO2='"+APELLIDO2+"',NACIONALIDAD='"+NACIONALIDAD+"',FECHA_NACIMIENTO='"+FECHA_NACIMIENTO+"',BIOGRAFIA'"+BIOGRAFIA    + "'  WHERE ID = '" + CODIGO + "'";
		           // Statement st = Conexion.createStatement();
		          //  st.executeUpdate(QueryAutor);
		            

		            

		     //   } catch (SQLException ex) {
		      //      System.out.println(ex.getMessage());
		       //     JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		      //  }
		  // 
		 
}
}
