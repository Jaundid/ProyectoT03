package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.Toolkit;

import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class UsuarioEditar extends JFrame {
	private String dbname="";
	private String user="oracle";
	private String pass="root";
	private static Connection Conexion=null;
	private JPanel contentPane;
	private JTextField tfNick;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTextField tfemail;
	private JTextField tftelefono;
	private JTextField tffechaNacimiento;
	private JTextField tfPreferencia;
	private JTextField tfPassword;
	private JTextField tfApellido2;
	private JTextField tfDNI;
	private JTextField tfBiblioteca;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuarioEditar frame = new UsuarioEditar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UsuarioEditar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Men\u00FA Usuarios Editar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,  800, 640);
		setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblCodUsuario = new JLabel("Cod_Usuario");
		lblCodUsuario.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblCodUsuario, 70, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblCodUsuario, 42, SpringLayout.WEST, contentPane);
		lblCodUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblCodUsuario);
		
		final JComboBox comboBox = new JComboBox();
		sl_contentPane.putConstraint(SpringLayout.NORTH, comboBox, -1, SpringLayout.NORTH, lblCodUsuario);
		contentPane.add(comboBox);
		
		JLabel lblNickname = new JLabel("Nickname");
		lblNickname.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNickname, 16, SpringLayout.SOUTH, lblCodUsuario);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNickname, 42, SpringLayout.WEST, contentPane);
		lblNickname.setBounds(new Rectangle(70, 0, 0, 0));
		lblNickname.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblNickname);
		
		tfNick = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, comboBox, 0, SpringLayout.WEST, tfNick);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, comboBox, -8, SpringLayout.NORTH, tfNick);
		sl_contentPane.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, tfNick);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfNick, 86, SpringLayout.EAST, lblNickname);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfNick, -3, SpringLayout.NORTH, lblNickname);
		tfNick.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfNick.setColumns(10);
		contentPane.add(tfNick);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNombre, 42, SpringLayout.WEST, contentPane);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblNombre);
		
		tfNombre = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfNombre, -3, SpringLayout.NORTH, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfNombre, 101, SpringLayout.EAST, lblNombre);
		tfNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfNombre.setColumns(10);
		contentPane.add(tfNombre);
		
		JLabel lblApellido = new JLabel("Apellido1");
		lblApellido.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblApellido, 22, SpringLayout.SOUTH, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblApellido, 42, SpringLayout.WEST, contentPane);
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblApellido);
		
		tfApellido = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfApellido, -3, SpringLayout.NORTH, lblApellido);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfApellido, 89, SpringLayout.EAST, lblApellido);
		tfApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfApellido.setColumns(10);
		contentPane.add(tfApellido);
		
		JLabel lblemail = new JLabel("Email");
		lblemail.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblemail, 41, SpringLayout.WEST, contentPane);
		lblemail.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblemail);
		
		tfemail = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblemail, 3, SpringLayout.NORTH, tfemail);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfemail, 309, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfemail, 202, SpringLayout.WEST, contentPane);
		tfemail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfemail.setColumns(10);
		contentPane.add(tfemail);
		
		JLabel lbltelefono = new JLabel("Tel\u00E9fono");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lbltelefono, 21, SpringLayout.SOUTH, lblemail);
		lbltelefono.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lbltelefono, 42, SpringLayout.WEST, contentPane);
		lbltelefono.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lbltelefono);
		
		tftelefono = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tftelefono, -3, SpringLayout.NORTH, lbltelefono);
		sl_contentPane.putConstraint(SpringLayout.WEST, tftelefono, 95, SpringLayout.EAST, lbltelefono);
		tftelefono.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tftelefono.setColumns(10);
		contentPane.add(tftelefono);
		
		JLabel lblfechanacimiento = new JLabel("Fecha Nacimiento");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblfechanacimiento, 25, SpringLayout.SOUTH, lbltelefono);
		lblfechanacimiento.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblCodUsuario, 0, SpringLayout.EAST, lblfechanacimiento);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblfechanacimiento, 0, SpringLayout.WEST, lblemail);
		lblfechanacimiento.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblfechanacimiento);
		
		tffechaNacimiento = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tffechaNacimiento, -3, SpringLayout.NORTH, lblfechanacimiento);
		sl_contentPane.putConstraint(SpringLayout.WEST, tffechaNacimiento, 28, SpringLayout.EAST, lblfechanacimiento);
		tffechaNacimiento.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tffechaNacimiento.setColumns(10);
		contentPane.add(tffechaNacimiento);
		
		JLabel lblPreferencia = new JLabel("Preferencias");
		lblPreferencia.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblPreferencia, 26, SpringLayout.SOUTH, lblfechanacimiento);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblPreferencia, 42, SpringLayout.WEST, contentPane);
		lblPreferencia.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblPreferencia);
		
		tfPreferencia = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfPreferencia, 11, SpringLayout.SOUTH, tffechaNacimiento);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfPreferencia, 202, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, tfPreferencia, -81, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, tfPreferencia, -301, SpringLayout.EAST, contentPane);
		tfPreferencia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfPreferencia.setColumns(10);
		contentPane.add(tfPreferencia);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnEditar, 6, SpringLayout.SOUTH, tfPreferencia);
		contentPane.add(btnEditar);
		
		JButton btnAtras = new JButton("Atr\u00E1s");
		sl_contentPane.putConstraint(SpringLayout.WEST, btnEditar, 97, SpringLayout.EAST, btnAtras);
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnAtras, 57, SpringLayout.SOUTH, lblPreferencia);
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		sl_contentPane.putConstraint(SpringLayout.WEST, btnAtras, 0, SpringLayout.WEST, lblCodUsuario);
		contentPane.add(btnAtras);
		
		JLabel label = new JLabel("Apellido2");
		label.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.NORTH, label, 19, SpringLayout.SOUTH, lblApellido);
		sl_contentPane.putConstraint(SpringLayout.WEST, label, 42, SpringLayout.WEST, contentPane);
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(label);
		
		tfApellido2 = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfApellido2, -3, SpringLayout.NORTH, label);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfApellido2, 89, SpringLayout.EAST, label);
		tfApellido2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfApellido2.setColumns(10);
		contentPane.add(tfApellido2);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblPassword, 42, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNombre, 21, SpringLayout.SOUTH, lblPassword);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblPassword, 21, SpringLayout.SOUTH, lblNickname);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblPassword);
		
		tfPassword = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfPassword, -3, SpringLayout.NORTH, lblPassword);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfPassword, 87, SpringLayout.EAST, lblPassword);
		tfPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfPassword.setColumns(10);
		contentPane.add(tfPassword);
		
		JLabel lblDNI = new JLabel("DNI");
		lblDNI.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblDNI, 377, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblDNI, 0, SpringLayout.NORTH, lblNombre);
		lblDNI.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblDNI);
		
		tfDNI = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfDNI, -3, SpringLayout.NORTH, lblNombre);
		tfDNI.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfDNI.setColumns(10);
		contentPane.add(tfDNI);
		
		JButton btnBorrar = new JButton("Borrar");
		sl_contentPane.putConstraint(SpringLayout.EAST, btnEditar, -127, SpringLayout.WEST, btnBorrar);
		
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnBorrar, 0, SpringLayout.NORTH, btnEditar);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnBorrar, 0, SpringLayout.EAST, tfPreferencia);
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(btnBorrar);
		
		JLabel lblBiblioteca = new JLabel("Biblioteca");
		lblBiblioteca.setForeground(Color.WHITE);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblBiblioteca, 0, SpringLayout.WEST, lblDNI);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblBiblioteca, 0, SpringLayout.SOUTH, comboBox);
		lblBiblioteca.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane.add(lblBiblioteca);
		
		tfBiblioteca = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, tfDNI, 0, SpringLayout.WEST, tfBiblioteca);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tfBiblioteca, 1, SpringLayout.NORTH, lblBiblioteca);
		sl_contentPane.putConstraint(SpringLayout.WEST, tfBiblioteca, 6, SpringLayout.EAST, lblBiblioteca);
		tfBiblioteca.setEditable(false);
		contentPane.add(tfBiblioteca);
		tfBiblioteca.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(UsuarioEditar.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		contentPane.add(lblNewLabel);
		
		//"metodo" que rellena el combo box con los codigos
		try {
			SQLUsuarios db = new SQLUsuarios();
			db.SQLConnection("oracle", "root", "");
			String QueryUser = "SELECT CODIGO FROM USUARIO";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeUpdate(QueryUser);
            ResultSet rs = st.executeQuery(QueryUser);
            while(rs.next()){
            	
            	comboBox.addItem(rs.getObject("CODIGO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		//action listeners
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLUsuarios db = new SQLUsuarios();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.mostrarTupla(comboBox.getSelectedItem(), tfDNI.getText(), tfNick.getText(), tfNombre.getText(), tfApellido.getText(), tfApellido2.getText(), tfemail.getText(), tftelefono.getText(), tffechaNacimiento.getText(), tfPreferencia.getText(), tfPassword.getText());
				
			}
		});
		
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLUsuarios db = new SQLUsuarios();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.deleteRecord(comboBox.getSelectedItem());
			}
		});
		
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
		            String QueryUsuario = "SELECT * FROM USUARIO WHERE CODIGO = "+comboBox.getSelectedItem();
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryUsuario);
				//ghasd
				
		            while (resultSet.next()) {
		            	
		            	
		            	
		            	tfDNI.setText(resultSet.getString("DNI"));
		            	tfApellido.setText(resultSet.getString("APELLIDO_1"));
		            	tfApellido2.setText(resultSet.getString("APELLIDO_2"));
		            	tfNombre.setText(resultSet.getString("NOMBRE"));
		            	tfNick.setText(resultSet.getString("NICKNAME"));
		            	tfPassword.setText(resultSet.getString("PASS"));
		            	tfemail.setText(resultSet.getString("EMAIL"));
		            	tftelefono.setText(resultSet.getString("TELEFONO"));
		            	tffechaNacimiento.setText(resultSet.getString("FCHANACIMIENTO"));
		            	tfBiblioteca.setText(resultSet.getString("BIBLIOTECA"));
		            	tfPreferencia.setText(resultSet.getString("PREFERENCIA"));
		            	
		            	
		            	
		            	
		            	
		               /* System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
		                        + "Nombre: " + resultSet.getString("NOMBRE") + " " 
								+ resultSet.getString("APELLIDO1") + " "+resultSet.getString("APELLIDO2")
		                        + "Nacionalidad: " + resultSet.getString("NACIONALIDAD") + " "
		                        + "Fecha_Nacimiento: " + resultSet.getString("FECHA_NACIMIENTO")+" "+
		                        "Biografia: "+ resultSet.getString("BIOGRAFIA"));*/
		            }
				
				

				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				
			
		}
			
		});
				
				
		
		
	}
}
