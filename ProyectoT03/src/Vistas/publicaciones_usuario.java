package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class publicaciones_usuario extends JFrame {

	private JPanel contentPane;
	private JTextField tfArchivos;
	private JTextField tfIdioma;
	private JTextField tfFormato;
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	int i=0;
    int y=0;
    int x=0;
	private static Connection Conexion=null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					publicaciones_usuario frame = new publicaciones_usuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public publicaciones_usuario() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 385, 501);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Archivos");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 59, 103, 19);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("Autor");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 155, 89, 19);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("G\u00E9nero");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 196, 103, 19);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Idioma");
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_5.setBounds(40, 239, 89, 19);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Formato");
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_6.setBounds(40, 275, 89, 19);
		contentPane.add(lblNewLabel_6);
		
		tfArchivos = new JTextField();
		tfArchivos.setBounds(200, 58, 136, 25);
		contentPane.add(tfArchivos);
		tfArchivos.setColumns(10);
		
		final JComboBox comboBoxAutor = new JComboBox();
		comboBoxAutor.setBounds(200, 156, 136, 20);
		contentPane.add(comboBoxAutor);
		
		final JComboBox comboBoxGenero = new JComboBox();
		comboBoxGenero.setBounds(200, 197, 136, 20);
		contentPane.add(comboBoxGenero);
		
		tfIdioma = new JTextField();
		tfIdioma.setBounds(200, 238, 136, 25);
		contentPane.add(tfIdioma);
		tfIdioma.setColumns(10);
		
		tfFormato = new JTextField();
		tfFormato.setBounds(200, 274, 136, 25);
		contentPane.add(tfFormato);
		tfFormato.setColumns(10);
		
		JButton btnInsertar = new JButton("Insertar");
		
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInsertar.setBounds(247, 430, 89, 23);
		contentPane.add(btnInsertar);
		
		JButton btnAtras = new JButton("Atr\u00E1s");
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtras.setBounds(40, 430, 89, 23);
		contentPane.add(btnAtras);
		
		JLabel lblNewLabel_7 = new JLabel("New label");
		lblNewLabel_7.setIcon(new ImageIcon(publicaciones_usuario.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_7.setBounds(0, 0, 369, 463);
		contentPane.add(lblNewLabel_7);
		
		SQLAutor db = new SQLAutor();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryU = "SELECT NOMBRE FROM USUARIO ORDER BY CODIGO ASC";
			String QueryGE = "SELECT TIPO FROM GENERO ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement stU = Conexion.createStatement();
			Statement stG = Conexion.createStatement();
            stU.executeQuery(QueryU);
            stG.executeQuery(QueryGE);
            ResultSet rsA = stU.executeQuery(QueryU);
            ResultSet rsG = stG.executeQuery(QueryGE);
            while(rsA.next()){
            	
            	comboBoxAutor.addItem(rsA.getObject("NOMBRE"));
            }
            
            while(rsG.next()){
 	
            	comboBoxGenero.addItem(rsG.getObject("TIPO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLpublicaci�nUsuario db = new SQLpublicaci�nUsuario();
		        try {
					db.SQLConnection("oracle", "root", "");
					String QueryAutor = "SELECT CODIGO FROM USUARIO WHERE NOMBRE="+"'"+comboBoxAutor.getSelectedItem()+"'";
				
					Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
					
		         
		            Statement stA = Conexion.createStatement();
					
					
		            stA.executeQuery(QueryAutor);
		           
		            
		            ResultSet rsA = stA.executeQuery(QueryAutor);
		         
		            
		            while(rsA.next()){
		            	i=rsA.getInt("CODIGO");
		            }
		           
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        db.insertDataPubliUsuario(tfArchivos.getText(), i, comboBoxGenero.getSelectedItem(), tfIdioma.getText(), tfFormato.getText());       
		               
		        db.closeConnection();
			}
		});
	}
}
