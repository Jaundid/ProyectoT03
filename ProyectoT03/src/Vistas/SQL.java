/**
 * 
 */
package Vistas;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;





/**
 * @author Usuario
 *
 */
public class SQL {

	private static Connection Conexion = null;
	
//M�todo para iniciar sesi�n
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
        }
    }
     

	//M�todo QUE FINALIZA LA CONEXION A SQL
	public void closeConnection() {
	        try {
	            Conexion.close();
	            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
	        } catch (SQLException ex) {
	           
	            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
	        }
	    }
	
	/*public void createDB(String name) {
        try {
            String Query = "CREATE DATABASE " + name;
            Statement st = Conexion.createStatement();
            st.executeUpdate(Query);
            SQLConnection("root", "", name);
            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
        } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
	
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
	 public void createTable(String name) {
	        try {
	            String Query = "CREATE TABLE " + name + ""
	                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
	                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	            JOptionPane.showMessageDialog(null, "La tabla ya existe");
	        }
	    }
	
	//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
	 public void insertData(String table_name, String ID, String name, String lastname, String age, String gender) {
	        try {
	            String Query = "INSERT INTO " + table_name + " VALUES("
	            		+ "'"+ ID + "',"
	            		+ "'"+ name + "',"
	            		+ "'"+ lastname + "',"
	            		+ "'"+ age + "',"
	            		+ "'"+ gender + "')";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	    }	
	
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
	 public void getValues(String table_name) {
	        try {
	            String Query = "SELECT * FROM " + table_name;
	            Statement st = Conexion.createStatement();
	            java.sql.ResultSet resultSet;
	            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println("ID: " + resultSet.getString("ID") + " "
	                        + "Nombre: " + resultSet.getString("Nombre") + " " 
							+ resultSet.getString("Apellido") + " "
	                        + "Edad: " + resultSet.getString("Edad") + " "
	                        + "Sexo: " + resultSet.getString("Sexo"));
	            }

	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
	        }
	    }	
	
		
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord(String table_name, String ID) {
	        try {
	            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);

	        } catch (SQLException ex) {
	            System.out.println(ex.getMessage());
	            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
	    }
	
	
	 
	 
}


