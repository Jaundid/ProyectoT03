package Vistas;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Libros extends JFrame {
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	int i=0;
    int y=0;
    int x=0;
	private static Connection Conexion=null;
	private JTextField textFieldISBN;
	private JTextField textFieldTitulo;
	private JTextField textFieldAno;
	private JTextField textFieldIdioma;
	private JTextField textFieldFormato;
	private JTextField textFieldResumen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Libros frame = new Libros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Libros() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Insertar Libros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 671);
		getContentPane().setLayout(null);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setForeground(Color.WHITE);
		lblIsbn.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblIsbn.setBounds(40, 115, 81, 14);
		getContentPane().add(lblIsbn);
		
		JLabel lblNewLabel = new JLabel("T\u00EDtulo");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(40, 155, 81, 20);
		getContentPane().add(lblNewLabel);
		
		JLabel lblAo = new JLabel("Autor");
		lblAo.setForeground(Color.WHITE);
		lblAo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAo.setBounds(40, 195, 81, 20);
		getContentPane().add(lblAo);
		
		JLabel lblNewLabel_1 = new JLabel("A\u00F1o");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 235, 81, 20);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblFormato = new JLabel("G\u00E9nero");
		lblFormato.setForeground(Color.WHITE);
		lblFormato.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFormato.setBounds(40, 315, 60, 20);
		getContentPane().add(lblFormato);
		
		JLabel lblResumen = new JLabel("Idioma");
		lblResumen.setForeground(Color.WHITE);
		lblResumen.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblResumen.setBounds(40, 355, 81, 20);
		getContentPane().add(lblResumen);
		
		JButton btnInsertar = new JButton("Insertar");	
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnInsertar.setBounds(480, 563, 89, 23);
		getContentPane().add(btnInsertar);
		
		JButton btnAtrs = new JButton("Atr\u00E1s");
		btnAtrs.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtrs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	
		
		btnAtrs.setBounds(40, 563, 89, 23);
		getContentPane().add(btnAtrs);
		
		JLabel lblNewLabel_2 = new JLabel("Editorial");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 275, 81, 20);
		getContentPane().add(lblNewLabel_2);
		
		textFieldISBN = new JTextField();
		textFieldISBN.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldISBN.setColumns(10);
		textFieldISBN.setBounds(200, 110, 136, 25);
		getContentPane().add(textFieldISBN);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldTitulo.setColumns(10);
		textFieldTitulo.setBounds(200, 150, 136, 25);
		getContentPane().add(textFieldTitulo);
		
		textFieldAno = new JTextField();
		textFieldAno.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldAno.setColumns(10);
		textFieldAno.setBounds(200, 233, 136, 25);
		getContentPane().add(textFieldAno);
		
		textFieldIdioma = new JTextField();
		textFieldIdioma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldIdioma.setColumns(10);
		textFieldIdioma.setBounds(200, 350, 136, 25);
		getContentPane().add(textFieldIdioma);
		
		JLabel lblFormato_1 = new JLabel("Formato");
		lblFormato_1.setForeground(Color.WHITE);
		lblFormato_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFormato_1.setBounds(40, 395, 81, 25);
		getContentPane().add(lblFormato_1);
		
		textFieldFormato = new JTextField();
		textFieldFormato.setBounds(200, 395, 136, 25);
		getContentPane().add(textFieldFormato);
		textFieldFormato.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Resumen");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 435, 105, 23);
		getContentPane().add(lblNewLabel_3);
		
		textFieldResumen = new JTextField();
		textFieldResumen.setBounds(200, 431, 369, 108);
		getContentPane().add(textFieldResumen);
		textFieldResumen.setColumns(10);
		
		final JComboBox<Object> comboBoxAutor = new JComboBox();
		comboBoxAutor.setBounds(200, 189, 136, 30);
		getContentPane().add(comboBoxAutor);
		final JComboBox<Object> comboBoxEditorial = new JComboBox();
		comboBoxEditorial.setBounds(200, 266, 136, 30);
		getContentPane().add(comboBoxEditorial);
		final JComboBox<Object> comboBoxGenero = new JComboBox<Object>();
		comboBoxGenero.setBounds(200, 309, 136, 30);
		getContentPane().add(comboBoxGenero);
		
		JLabel lblNewLabel_4 = new JLabel("New label");
		lblNewLabel_4.setIcon(new ImageIcon(Libros.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_4.setBounds(0, 0, 784, 633);
		getContentPane().add(lblNewLabel_4);
		
		SQLAutor db = new SQLAutor();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT NOMBRE FROM AUTOR ORDER BY CODIGO ASC";
			String QueryED = "SELECT NOMBRE FROM EDITORIAL ORDER BY CODIGO ASC";
			String QueryGE = "SELECT TIPO FROM GENERO ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement stA = Conexion.createStatement();
			Statement stE = Conexion.createStatement();
			Statement stG = Conexion.createStatement();
            stA.executeQuery(QueryAutor);
            stE.executeQuery(QueryED);
            stG.executeQuery(QueryGE);
            ResultSet rsA = stA.executeQuery(QueryAutor);
            ResultSet rsE = stE.executeQuery(QueryED);
            ResultSet rsG = stG.executeQuery(QueryGE);
            while(rsA.next()){
            	
            	comboBoxAutor.addItem(rsA.getObject("NOMBRE"));
            }
            while(rsE.next()){
            	
            	comboBoxEditorial.addItem(rsE.getObject("NOMBRE"));
            }
            while(rsG.next()){
 	
            	comboBoxGenero.addItem(rsG.getObject("TIPO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*Comentado de momento
		comboBoxAutor.setBounds(200, 189, 136, 30);
		getContentPane().add(comboBoxAutor);
		
		final JComboBox<Object> comboBoxEditorial = new JComboBox();
		SQLLibros db1 = new SQLLibros();
		try {
			db1.SQLConnection("oracle", "root", "");
			String QueryED = "SELECT NOMBRE FROM EDITORIAL ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryED);
            ResultSet rs = st.executeQuery(QueryED);
            while(rs.next()){
            	
            	comboBoxEditorial.addItem(rs.getObject("NOMBRE"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		comboBoxEditorial.setBounds(200, 266, 136, 30);
		getContentPane().add(comboBoxEditorial);
		final JComboBox<Object> comboBoxGenero = new JComboBox<Object>();
		SQLAutor db2 = new SQLAutor();
		try {
			db2.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT TIPO FROM GENERO ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryAutor);
            ResultSet rs = st.executeQuery(QueryAutor);
            while(rs.next()){
            	
            	comboBoxGenero.addItem(rs.getObject("TIPO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		comboBoxGenero.setBounds(200, 309, 136, 30);
		getContentPane().add(comboBoxGenero);
		*/
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLLibros db = new SQLLibros();
		        try {
					db.SQLConnection("oracle", "root", "");
					String QueryAutor = "SELECT CODIGO FROM AUTOR WHERE NOMBRE="+"'"+comboBoxAutor.getSelectedItem()+"'";
					String QueryED = "SELECT CODIGO FROM EDITORIAL WHERE NOMBRE="+"'"+comboBoxEditorial.getSelectedItem()+"'";
					String QueryGE = "SELECT CODIGO FROM GENERO WHERE TIPO="+"'"+comboBoxGenero.getSelectedItem()+"'";
					Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
					
		         
		            Statement stA = Conexion.createStatement();
					Statement stE = Conexion.createStatement();
					Statement stG = Conexion.createStatement();
		            stA.executeQuery(QueryAutor);
		            stE.executeQuery(QueryED);
		            stG.executeQuery(QueryGE);
		            ResultSet rsA = stA.executeQuery(QueryAutor);
		            ResultSet rsE = stE.executeQuery(QueryED);
		            ResultSet rsG = stG.executeQuery(QueryGE);
		            
		            while(rsA.next()){
		            	i=rsA.getInt("CODIGO");
		            }
		            while(rsE.next()){
		            	y=rsE.getInt("CODIGO");
		            }
		            while(rsG.next()){
		            	 x=rsG.getInt("CODIGO");
		            }
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       db.insertDataLibros(textFieldISBN.getText(), textFieldTitulo.getText(), i, textFieldAno.getText(),y, x, textFieldIdioma.getText(), textFieldFormato.getText(), textFieldResumen.getText());
		       String c="seq_Libro.currval";
		       db.insertDataEscribe(i, c);        
		       db.insertDataPUBLICA(y,c);         
		               
		        db.closeConnection();
		      
				
			}

			
		});
		
	}
}
