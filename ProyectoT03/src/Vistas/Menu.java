package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Window.Type;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class Menu extends JFrame {

	private JPanel contentPane;
	private String Usuario="admin";
	private String pass="1234";
	private JTextField tfusuario;
	private JPasswordField pfield;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setForeground(Color.BLACK);
		setTitle("MENU");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 593, 402);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 774, 21);
		contentPane.add(menuBar);
		
		final JMenu mnIngresar = new JMenu("Ingresar");
		menuBar.add(mnIngresar);
		
		JMenuItem mntmAutores = new JMenuItem("Autores");
		mntmAutores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Autores ai= new Autores();
				ai.setVisible(true);
			}
		});
		mnIngresar.add(mntmAutores);
		
		JMenuItem mntmEditoriales = new JMenuItem("Editoriales");
		mntmEditoriales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Editoriales ed= new Editoriales();
				ed.setVisible(true);
			}
		});
		mnIngresar.add(mntmEditoriales);
		
		JMenuItem mntmLibros = new JMenuItem("Libros");
		mntmLibros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Libros li= new Libros();
				li.setVisible(true);
 			}
		});
		mnIngresar.add(mntmLibros);
		
		JMenuItem mntmBiblioteca = new JMenuItem("Biblioteca");
		mntmBiblioteca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Biblioteca bi= new Biblioteca();
				bi.setVisible(true);
			}
		});
		mnIngresar.add(mntmBiblioteca);
		
		JMenuItem mntmClaisifican = new JMenuItem("Clasifican");
		mntmClaisifican.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clasifican cl= new Clasifican();
				cl.setVisible(true);
			}
		});
		mnIngresar.add(mntmClaisifican);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("�rdenes");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				�rdenes or= new �rdenes();
				or.setVisible(true);
			}
		});
		
		JMenuItem mntmGenero = new JMenuItem("Genero");
		mntmGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Genero Ge= new Genero();
				Ge.setVisible(true);
			}
		});
		mnIngresar.add(mntmGenero);
		mnIngresar.add(mntmNewMenuItem);
		
		JMenuItem mntmPublica = new JMenuItem("Publica");
		mntmPublica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Publica pu= new Publica();
				pu.setVisible(true);
			}
		});
		mnIngresar.add(mntmPublica);
		
		JMenuItem mntmUsuario = new JMenuItem("Usuario");
		mntmUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario us= new Usuario();
				us.setVisible(true);
			}
		});
		mnIngresar.add(mntmUsuario);
		
		JMenuItem mntmEscribe = new JMenuItem("Escribe");
		mntmEscribe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Escribe es= new Escribe();
				es.setVisible(true);
			}
		});
		mnIngresar.add(mntmEscribe);
		
		final JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem menuItem = new JMenuItem("Autores");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AutoresEdicion aied=new AutoresEdicion();
				aied.setVisible(true);
			}
		});
		mnEditar.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Editoriales");
		menuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditorialesEditar eded= new EditorialesEditar();
				eded.setVisible(true);
			}
		});
		mnEditar.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Libros");
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LibrosEditar lied= new LibrosEditar();
				lied.setVisible(true);
			}
		});
		mnEditar.add(menuItem_2);
		
		JMenuItem menuItem_3 = new JMenuItem("Biblioteca");
		menuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Biblioteca_Edicion bied= new Biblioteca_Edicion();
				bied.setVisible(true);
			}
		});
		mnEditar.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("Clasifican");
		menuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clasifican cl= new Clasifican();
				cl.setVisible(true);
			}
		});
		mnEditar.add(menuItem_4);
		
		JMenuItem menuItem_5 = new JMenuItem("\u00D3rdenes");
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				�rdenes or=new �rdenes();
				or.setVisible(true);
			}
		});
		
		JMenuItem mntmGenero_1 = new JMenuItem("Genero");
		mnEditar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			GeneroEditar GeEd=new GeneroEditar();
			GeEd.setVisible(true);
		}
	});
	
		JMenuItem menuItem_6 = new JMenuItem("Publica");
		menuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Publica pu=new Publica();
				pu.setVisible(true);
			}
		});
		
		JMenuItem menuItem_5_1 = new JMenuItem("Genero");
		mnEditar.add(menuItem_5_1);
		mnEditar.add(menuItem_6);
		
		JMenuItem menuItem_7 = new JMenuItem("Usuario");
		menuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UsuarioEditar used=new UsuarioEditar();
				used.setVisible(true);
			}
		});
		mnEditar.add(menuItem_7);
		
		JMenuItem menuItem_8 = new JMenuItem("Escribe");
		menuItem_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Escribe es=new Escribe();
				es.setVisible(true);
			}
		});
		mnEditar.add(menuItem_8);
		
		mnIngresar.setEnabled(false);
		mnEditar.setEnabled(false);
		
		final JLabel lblLogin = new JLabel("Login");
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblLogin.setBounds(245, 133, 53, 37);
		contentPane.add(lblLogin);
		
		final JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUsuario.setBounds(120, 179, 76, 21);
		contentPane.add(lblUsuario);
		
		final JLabel labelpass = new JLabel("Password");
		labelpass.setForeground(Color.WHITE);
		labelpass.setFont(new Font("Tahoma", Font.BOLD, 15));
		labelpass.setBounds(120, 224, 91, 14);
		contentPane.add(labelpass);
		
		tfusuario = new JTextField();
		tfusuario.setBounds(221, 181, 86, 21);
		contentPane.add(tfusuario);
		tfusuario.setColumns(10);
		
		pfield = new JPasswordField();
		pfield.setBounds(221, 223, 86, 21);
		contentPane.add(pfield);
		
		final JButton btndesloguear = new JButton("DESLOGUEAR");
		btndesloguear.setFont(new Font("Tahoma", Font.BOLD, 11));
		btndesloguear.setBounds(202, 261, 118, 37);
		contentPane.add(btndesloguear);
		btndesloguear.setVisible(false);
		
		final JButton btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfusuario.getText().equalsIgnoreCase(Usuario) && pfield.getText().equalsIgnoreCase(pass)){
					mnIngresar.setEnabled(true);
					mnEditar.setEnabled(true);
					btnLogin.setVisible(false);
					btndesloguear.setVisible(true);
					lblUsuario.setVisible(false);
					labelpass.setVisible(false);
					tfusuario.setVisible(false);
					pfield.setVisible(false);
					lblLogin.setVisible(false);
					JOptionPane.showMessageDialog(null, "Bienvenido "+Usuario);
				}else {
					JOptionPane.showMessageDialog(null, "Error de usuario ");
				}
			}
		});
		btnLogin.setBounds(221, 261, 89, 37);
		contentPane.add(btnLogin);
		
		JLabel lblBienvenido = new JLabel("BIENVENIDOS A BITERATURA!");
		lblBienvenido.setForeground(Color.WHITE);
		lblBienvenido.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblBienvenido.setBounds(74, 81, 433, 46);
		contentPane.add(lblBienvenido);
		
		
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Menu.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 20, 577, 343);
		contentPane.add(lblNewLabel);
		
		btndesloguear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mnIngresar.setEnabled(false);
				mnEditar.setEnabled(false);
				btnLogin.setVisible(true);
				btndesloguear.setVisible(false);
				lblUsuario.setVisible(true);
				labelpass.setVisible(true);
				tfusuario.setVisible(true);
				pfield.setVisible(true);
				lblLogin.setVisible(true);
				JOptionPane.showMessageDialog(null, "Adios! ");
				
			}
		});
	
		
	}
}
