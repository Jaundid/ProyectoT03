package Vistas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQLGenero {
	private static Connection Conexion = null;
	
	//M閠odo para iniciar sesi髇
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi贸n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi贸n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi贸n con el servidor");
	        }
	    }
	     

		//M閠odo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
			
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		 public void createTable(String name) {
		        try {
		        	 String Query = "CREATE TABLE " + name + ""
			                    + "(CODIGO VARCHAR(25),TIPO VARCHAR(50))";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla" + name + "de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertData(String TIPO) {
		        try {
		            String QueryGenero = "INSERT INTO GENERO VALUES("
		            		+"seq_Genero.nextval"+","		            		            		
		            		+ "'"+ TIPO + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryGenero);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValues() {
		        try {
		            String QueryAutor = "SELECT * FROM GENERO";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryAutor);

		            while (resultSet.next()) {
		                System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
		                        + "Tipo:  "+ resultSet.getString("TIPO"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici贸n de datos");
		        }
		    }	
		
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(Object object) {
		        try {
		            String QueryAutor = "DELETE FROM GENERO WHERE Codigo = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void alterRecord(String CODIGO, String TIPO) {
		        try {
		            String QueryGenero = "UPDATE Genero set TIPO='"+TIPO+"'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryGenero);
		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 
		 
		 //llenar combobox
		 public String getValues(String Genero) {
		        try {
		            String QueryGenero = "SELECT CODIGO FROM " + Genero;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryGenero);

		            while (resultSet.next()) {
		             
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici贸n de datos");
		        }
				return Genero;
		    }	
		 
		 
		 
		 public void mostrarTupla( Object object, String TIPO) {
		        try {
		            String QueryGenero = "UPDATE TIPO set TIPO='"+TIPO + "'  WHERE CODIGO = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryGenero);
		            

		            

		      } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error EDITANDO el registro especificado");
		        }
		  
		 
		 
		
		 
}
}
