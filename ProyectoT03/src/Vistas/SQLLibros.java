package Vistas;
/**
 * 
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class SQLLibros {
	private static Connection Conexion = null;
	
	//M�todo para iniciar sesi�n
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        }
	    }
	     

		//M�todo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
		/*public void createDB(String name) {
	        try {
	            String Query = "CREATE DATABASE " + name;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            SQLConnection("root", "", name);
	            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }*/
		
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		/* public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		*/
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertDataLibros( String ISBN, String TITULO, Object AUTOR, String A�O, Object EDITORIAL, Object GENERO, String IDIOMA, String FORMATO, String RESUMEN) {
		        try {
		            String QueryLibros = "INSERT INTO LIBRO VALUES("
		            		//+ "'"+ CODIGO + "',"
		            		+"seq_libro.nextval"+","
		            		+ "'"+ ISBN + "',"
		            		+ "'"+ TITULO + "',"
		            		+"'"+ AUTOR +"',"
		            		+ "'"+ A�O + "',"
		            		+ "'"+ EDITORIAL + "',"
		            		+ "'"+ GENERO + "',"
		            		+ "'"+ IDIOMA + "',"
		            		+ "'"+ FORMATO + "',"
		            		+ "'"+ RESUMEN + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }
		 public void insertDataEscribe(int CODIGOAUTOR, String c) {
		        try {
		            String QueryLibros = "INSERT INTO ESCRIBE VALUES("
		            		+ CODIGOAUTOR + ","
		            		+ c+ ")";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 public void insertDataPUBLICA(int CODIGOEDITORIAL, String c) {
		        try {
		            String QueryLibros = "INSERT INTO PUBLICA VALUES("
		            		+ CODIGOEDITORIAL + ","
		            		+ c+ ")";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }
		 
		 public void insertDataOrdenes( Object object, int CODLIBRO) {
		        try {
		            String QueryLibros = "INSERT INTO ORDENES VALUES("
		            		+"seq_Ordenes.nextval" + ","
		            		+"SYSDATE"+"," 
		            		+object+","
		            		+"0"+","
		            		+CODLIBRO+")";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }
		 public void insertDataBIBLIO(String string, int CODIGOLIBRO,String string2) {
		        try {
		            String QueryUsuario = "INSERT INTO BIBLIOTECA VALUES("
		            		
		            		+string+","
		            		+CODIGOLIBRO+","
		            		+string2+")"
		            		;
		            Statement st = Conexion.createStatement();
		            st.executeQuery(QueryUsuario);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }
		/* public void insertDataLibro(String CODIGO, String ISBN, String TITULO, String A�O, String EDITORIAL, String GENERO, String IDIOMA, String FORMATO, String RESUMEN) {
		        try {
		            String QueryLibro = "INSERT INTO LIBRO VALUES("
		            		+ "'"+ CODIGO + "',"
		            		+ "'"+ IDIOMA + "',"
		            		+ "'"+ TITULO + "',"
		            		+ "'"+ A�O + "',"
		            		+ "'"+ EDITORIAL + "',"
		            		+ "'"+ GENERO + "',"		            		
		            		+ "'"+ FORMATO + "',"
		            		+ "'"+ RESUMEN + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibro);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }*/
		
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		/* public void getValuesLibros() {
		        try {
		            String QueryLibros = "SELECT * FROM Libros";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryLibros);

		            while (resultSet.next()) {
		                System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
		                        + "ISBN: " + resultSet.getString("ISBN") + " " 
								+ resultSet.getString("T�tulo") + " "+resultSet.getString("TITULO")
		                        + "A�o: " + resultSet.getString("A�O") + " "
		                        + "Editorial: " + resultSet.getString("EDITORIAL")+" "+
		                        "G�nero: "+ resultSet.getString("GENERO")
		                        + "Formato: " + resultSet.getString("FORMATO")+" "
		                		+ "Resumen: " + resultSet.getString("RESUMEN")+" ");
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        }
		    }	
		*/
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(Object object) {
		        try {
		            String QueryLibros = "DELETE FROM Libro WHERE codigo = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void alterRecord(Object object, String ISBN, String TITULO, Object AUTOR, String A�O, Object EDITORIAL, Object GENERO, String IDIOMA, String FORMATO, String RESUMEN) {
		        try {
		            String QueryLibros = "UPDATE LIBRO set ISBN='"+ISBN +"',TITULO='"+TITULO +"',AUTOR='"+AUTOR+"',A�O='"+A�O+"',EDITORIAL='"+EDITORIAL+"',GENERO='"+GENERO+"',IDIOMA='"+IDIOMA+"',FORMATO='"+FORMATO+"',RESUMEN='"+RESUMEN + "'  WHERE CODIGO = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryLibros);
		            

		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error editando el registro especificado");
		        }
		    }
		 
		/* public void mostarTupla(Object object, String ISBN, String TITULO, String AUTOR, String A�O, String EDITORIAL, String GENERO, String IDIOMA, String FORMATO, String RESUMEN) {
		       try {
		            String QueryLibros = "UPDATE Libro set ISBN='"+ISBN +"',TITULO='"+TITULO+"',AUTOR="+AUTOR+"',A�O='"+A�O+"',EDITORIAL='"+EDITORIAL+"',GENERO='"+GENERO+"',GENERO="+GENERO+"',FORMATO='"+FORMATO+"',RESUMEN='"+RESUMEN+"'  WHERE codigo = '" + object + "'";
		            Statement st = Conexion.createStatement();
		           st.executeUpdate(QueryLibros);
		            

		            

		      } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		           JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }*/
		  
		 
}

