/**
 * 
 */
package Vistas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.*;
import javax.swing.JOptionPane;

/**
 * @author Usuari
 *
 */
public class SQLUsuarios {
	private static Connection Conexion = null;

	//M�todo para iniciar sesi�n
		public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexión con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexión con el servidor");
	        }
	    }
	     

		//M�todo QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexion con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexion con el servidor");
		        }
		    }
		
		/*public void createDB(String name) {
	        try {
	            String Query = "CREATE DATABASE " + name;
	            Statement st = Conexion.createStatement();
	            st.executeUpdate(Query);
	            SQLConnection("root", "", name);
	            JOptionPane.showMessageDialog(null, "Se ha creado la base de datos " + name + " de forma exitosa");
	        } catch (SQLException ex) {
	            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }*/
		
		//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		/* public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		            JOptionPane.showMessageDialog(null, "La tabla ya existe");
		        }
		    }
		*/
		//METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOSSS
		 public void insertDataUsuario(String DNI, String NOMBRE,String APELLIDO1, String APELLIDO2, String NICKNAME, String PASS, String EMAIL, String TELEFONO, String FECHA_NACIMIENTO,String PREFERENCIAS) {
		        try {
		            String QueryUsuario = "INSERT INTO USUARIO VALUES"
		            		+"(seq_Usuario.nextval,"
		            		+ "'"+ DNI + "',"
		            		+ "'"+ NOMBRE + "',"
		            		+ "'"+ APELLIDO1 + "',"
		            		+ "'"+ APELLIDO2 + "',"
		            		+ "'"+ NICKNAME + "',"
		            		+ "'"+ PASS + "',"
		            		+ "'"+ EMAIL + "',"
		            		+ "'"+ TELEFONO + "',"
		            		+ "'"+ FECHA_NACIMIENTO + "',"
		            		+ "seq_Biblioteca.nextval,"
		            		+ "'"+ PREFERENCIAS + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryUsuario);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		
		//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValues() {
		        try {
		            String QueryUsuario = "SELECT * FROM USUARIO";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryUsuario);

		            while (resultSet.next()) {
		                System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
								+ resultSet.getString("Nickname") + " "+resultSet.getString("NICKNAME")
								+ "Nombre: " + resultSet.getString("NOMBRE") + " " 
		                        + "Apellido1: " + resultSet.getString("APELLIDO1") + " "
		                        + "Apellido2: " + resultSet.getString("APELLIDO2") + " " 
		                        + "Email: " + resultSet.getString("EMAIL") + " "
		                        + "Tel�fono: " + resultSet.getString("TELEFONO") + " " 
		                        + "Fecha_Nacimiento: " + resultSet.getString("FECHA_NACIMIENTO")+" "+
		                        "Preferencias: "+ resultSet.getString("PREFERENCIAS"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        }
		    }
		 
		 public void insertDataBIBLIO(String string, int CODIGOLIBRO,String string2) {
		        try {
		            String QueryUsuario = "INSERT INTO BIBLIOTECA VALUES("
		            		
		            		+string+","
		            		+CODIGOLIBRO+","
		            		+string2+")"
		            		;
		            Statement st = Conexion.createStatement();
		            st.executeQuery(QueryUsuario);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }
		 
		 /*public void insertDataBIBLIOordenes(Object codBiblio, int CODIGOLIBRO,Object codUsuario) {
		        try {
		            String QueryUsuario = "INSERT INTO BIBLIOTECA VALUES("
		            		
		            		+codBiblio+","
		            		+CODIGOLIBRO+","
		            		+codUsuario+")"
		            		;
		            Statement st = Conexion.createStatement();
		            st.executeQuery(QueryUsuario);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	*/
		
			
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(Object object) {
		        try {
		            String QueryAutor = "DELETE FROM USUARIO WHERE Codigo = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryAutor);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 //Altera table
		 public void mostrarTupla(Object object, String DNI, String NICKNAME, String NOMBRE, String APELLIDO1, String APELLIDO2, String EMAIL, String TELEFONO, String FECHA_NACIMIENTO, String PREFERENCIAS, String PASS) {
		        try {
		            String QueryUsuario = "UPDATE USUARIO set DNI='"+DNI +"',NOMBRE='"+NOMBRE +"',APELLIDO_1='"+APELLIDO1+"',APELLIDO_2='"+APELLIDO2+"',NICKNAME='"+NICKNAME+"',PASS='"+PASS+"',EMAIL='"+EMAIL+"',TELEFONO='"+TELEFONO+"',FCHANACIMIENTO='"+FECHA_NACIMIENTO    + "'  WHERE ID = '" + object + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(QueryUsuario);
		            

		            

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }
		 
		
}
