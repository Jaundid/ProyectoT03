package Vistas;



import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.awt.TextField;
import java.awt.Toolkit;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
public class AutoresEdicion extends JFrame {
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;
	private JPanel contentPane;
	private JPanel contentPane_1;
	private JTextField textField_Nombre;
	private JTextField textField_Apellido;
	private JTextField textField_FechaNacimiento;
	private JTextField textField_Biografia;
	private JTextField textFieldApellido2;
	private JTextField textFieldNacionalodad;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AutoresEdicion frame = new AutoresEdicion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AutoresEdicion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Editar Autor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 523, 372);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		//SpringLayout sl_contentPane = new SpringLayout();
		//contentPane.setLayout(sl_contentPane);
		
		
		contentPane_1 = new JPanel();
		contentPane_1.setBackground(Color.WHITE);
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cod_Autor");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(40, 39, 93, 14);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(40, 76, 83, 19);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setForeground(Color.WHITE);
		lblApellido.setBounds(40, 106, 83, 19);
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblApellido);
		
		JLabel lblFechanacimiento = new JLabel("Fecha_nacimiento");
		lblFechanacimiento.setForeground(Color.WHITE);
		lblFechanacimiento.setBounds(40, 199, 153, 19);
		lblFechanacimiento.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblFechanacimiento);
		
		JLabel lblBibliografa = new JLabel("Bibliograf\u00EDa");
		lblBibliografa.setForeground(Color.WHITE);
		lblBibliografa.setBounds(40, 229, 110, 19);
		lblBibliografa.setFont(new Font("Tahoma", Font.BOLD, 15));
		contentPane_1.add(lblBibliografa);
		
		JButton btnNewButton = new JButton("Atras");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
            dispose();
			}
		});
		btnNewButton.setBounds(40, 285, 89, 23);
		contentPane_1.add(btnNewButton);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnEditar.setBounds(163, 285, 89, 23);
		contentPane_1.add(btnEditar);
		
		textField_Nombre = new JTextField();
		textField_Nombre.setBackground(Color.WHITE);
		textField_Nombre.setBounds(203, 77, 86, 20);
		textField_Nombre.setColumns(10);
		contentPane_1.add(textField_Nombre);
		
		textField_Apellido = new JTextField();
		textField_Apellido.setBounds(203, 107, 86, 20);
		textField_Apellido.setColumns(10);
		contentPane_1.add(textField_Apellido);
		
		textFieldApellido2 = new JTextField();
		textFieldApellido2.setColumns(10);
		textFieldApellido2.setBounds(203, 138, 86, 20);
		contentPane_1.add(textFieldApellido2);
		
		textFieldNacionalodad = new JTextField();
		textFieldNacionalodad.setBounds(203, 169, 86, 20);
		contentPane_1.add(textFieldNacionalodad);
		textFieldNacionalodad.setColumns(10);
		
		textField_FechaNacimiento = new JTextField();
		textField_FechaNacimiento.setBounds(203, 200, 86, 20);
		textField_FechaNacimiento.setColumns(10);
		contentPane_1.add(textField_FechaNacimiento);
		
		textField_Biografia = new JTextField();
		textField_Biografia.setBounds(203, 230, 86, 20);
		textField_Biografia.setColumns(10);
		contentPane_1.add(textField_Biografia);
		
		final JComboBox<Object> comboBox = new JComboBox();
		
		
				SQLAutor db = new SQLAutor();
				try {
					db.SQLConnection("oracle", "root", "");
					String QueryAutor = "SELECT CODIGO FROM AUTOR ORDER BY CODIGO ASC";
					Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
					Statement st = Conexion.createStatement();
		            st.executeQuery(QueryAutor);
		            ResultSet rs = st.executeQuery(QueryAutor);
		            while(rs.next()){
		            	
		            	comboBox.addItem(rs.getObject("CODIGO"));
		            }
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				comboBox.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						try {
							
				            String QueryAutor = "SELECT * FROM AUTOR WHERE CODIGO = "+comboBox.getSelectedItem();
				            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(QueryAutor);
						
						
				            while (resultSet.next()) {
				            	
				            	
				            	
				            	textField_Nombre.setText(resultSet.getString("NOMBRE"));
				            	textField_Apellido.setText(resultSet.getString("APELLIDO_1"));
				            	textFieldApellido2.setText(resultSet.getString("APELLIDO_2"));
				            	textFieldNacionalodad.setText(resultSet.getString("NACIONALIDAD"));
				            	textField_FechaNacimiento.setText(resultSet.getString("FECHA_NACIMIENTO"));
				            	textField_Biografia.setText(resultSet.getString("BIOGRAFIA"));
				               /* System.out.println("Codigo: " + resultSet.getString("CODIGO") + " "
				                        + "Nombre: " + resultSet.getString("NOMBRE") + " " 
										+ resultSet.getString("APELLIDO1") + " "+resultSet.getString("APELLIDO2")
				                        + "Nacionalidad: " + resultSet.getString("NACIONALIDAD") + " "
				                        + "Fecha_Nacimiento: " + resultSet.getString("FECHA_NACIMIENTO")+" "+
				                        "Biografia: "+ resultSet.getString("BIOGRAFIA"));*/
				            }
						
						

						} catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
				        } 
						
					
				}
					
				});
			
				btnEditar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						SQLAutor db = new SQLAutor();
						try {
							db.SQLConnection("oracle", "root", "");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						db.mostrarTupla(comboBox.getSelectedItem(), textField_Nombre.getText(),textField_Apellido.getText(), textFieldApellido2.getText(), textFieldNacionalodad.getText(), textField_FechaNacimiento.getText(), textField_Biografia.getText());
					
					}
				});
		
		comboBox.setBounds(203, 38, 86, 20);
		contentPane_1.add(comboBox);
		
		
		
		
		
		
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnBorrar.setBounds(287, 285, 89, 23);
		contentPane_1.add(btnBorrar);
		
		JLabel labelApellido2 = new JLabel("Apellido2");
		labelApellido2.setForeground(Color.WHITE);
		labelApellido2.setFont(new Font("Tahoma", Font.BOLD, 15));
		labelApellido2.setBounds(40, 136, 83, 22);
		contentPane_1.add(labelApellido2);
		
		JLabel lblNewLabel_1 = new JLabel("Nacionalidad");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 169, 110, 19);
		contentPane_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon(AutoresEdicion.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_2.setBounds(0, 0, 507, 334);
		contentPane_1.add(lblNewLabel_2);
		
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLAutor db = new SQLAutor();
				db.deleteRecord(comboBox.getSelectedItem());
				JOptionPane.showMessageDialog(null, "Se ha borrado el registro");
			}
		});
		
	}
}
