package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Usuario extends JFrame {

	private JPanel tfApellido2;
	private JTextField tfNick;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTextField tfemail;
	private JTextField tftelefono;
	private JTextField tffechaNacimiento;
	private JTextField tfPreferencia;
	private JTextField txtApellido;
	private JTextField tfDNI;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Usuario frame = new Usuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Usuario() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Men\u00FA Usuarios");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,  800, 640);
		setVisible(true);
		tfApellido2 = new JPanel();
		tfApellido2.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(tfApellido2);
		SpringLayout sl_tfApellido2 = new SpringLayout();
		tfApellido2.setLayout(sl_tfApellido2);
		
		JLabel lblNickname = new JLabel("Nickname");
		lblNickname.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblNickname, 110, SpringLayout.NORTH, tfApellido2);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblNickname, 42, SpringLayout.WEST, tfApellido2);
		lblNickname.setBounds(new Rectangle(70, 0, 0, 0));
		lblNickname.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblNickname);
		
		tfNick = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfNick, -3, SpringLayout.NORTH, lblNickname);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tfNick, 86, SpringLayout.EAST, lblNickname);
		tfNick.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfNick.setColumns(10);
		tfApellido2.add(tfNick);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblNombre, 42, SpringLayout.WEST, tfApellido2);
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblNombre);
		
		tfNombre = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tfNombre, 0, SpringLayout.WEST, tfNick);
		tfNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfNombre.setColumns(10);
		tfApellido2.add(tfNombre);
		
		JLabel lblApellido = new JLabel("Apellido1");
		lblApellido.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblApellido, 22, SpringLayout.SOUTH, lblNombre);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblApellido, 42, SpringLayout.WEST, tfApellido2);
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblApellido);
		
		tfApellido = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfApellido, -3, SpringLayout.NORTH, lblNombre);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, tfApellido, 0, SpringLayout.EAST, tfNick);
		tfApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfApellido.setColumns(10);
		tfApellido2.add(tfApellido);
		
		JLabel lblemail = new JLabel("Email");
		lblemail.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblemail, 41, SpringLayout.WEST, tfApellido2);
		lblemail.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblemail);
		
		tfemail = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfemail, -3, SpringLayout.NORTH, lblemail);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tfemail, 120, SpringLayout.EAST, lblemail);
		tfemail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfemail.setColumns(10);
		tfApellido2.add(tfemail);
		
		JLabel lbltelefono = new JLabel("Tel\u00E9fono");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lbltelefono, 21, SpringLayout.SOUTH, lblemail);
		lbltelefono.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lbltelefono, 42, SpringLayout.WEST, tfApellido2);
		lbltelefono.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lbltelefono);
		
		tftelefono = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tftelefono, -3, SpringLayout.NORTH, lbltelefono);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tftelefono, 95, SpringLayout.EAST, lbltelefono);
		tftelefono.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tftelefono.setColumns(10);
		tfApellido2.add(tftelefono);
		
		JLabel lblfechanacimiento = new JLabel("Fecha Nacimiento");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblfechanacimiento, 25, SpringLayout.SOUTH, lbltelefono);
		lblfechanacimiento.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblfechanacimiento, 0, SpringLayout.WEST, lblemail);
		lblfechanacimiento.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblfechanacimiento);
		
		tffechaNacimiento = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tffechaNacimiento, -3, SpringLayout.NORTH, lblfechanacimiento);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tffechaNacimiento, 28, SpringLayout.EAST, lblfechanacimiento);
		tffechaNacimiento.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tffechaNacimiento.setColumns(10);
		tfApellido2.add(tffechaNacimiento);
		
		JLabel lblPreferencia = new JLabel("Preferencias");
		lblPreferencia.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblPreferencia, 26, SpringLayout.SOUTH, lblfechanacimiento);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblPreferencia, 42, SpringLayout.WEST, tfApellido2);
		lblPreferencia.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblPreferencia);
		
		tfPreferencia = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfPreferencia, 11, SpringLayout.SOUTH, tffechaNacimiento);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tfPreferencia, 202, SpringLayout.WEST, tfApellido2);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, tfPreferencia, -301, SpringLayout.EAST, tfApellido2);
		tfPreferencia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfPreferencia.setColumns(10);
		tfApellido2.add(tfPreferencia);
		
		JButton btnInsertar = new JButton("Insertar");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, btnInsertar, 517, SpringLayout.NORTH, tfApellido2);
		sl_tfApellido2.putConstraint(SpringLayout.SOUTH, tfPreferencia, -11, SpringLayout.NORTH, btnInsertar);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, btnInsertar, 0, SpringLayout.EAST, tfPreferencia);
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfApellido2.add(btnInsertar);
		
		JButton btnAtras = new JButton("Atr\u00E1s");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, btnAtras, 0, SpringLayout.NORTH, btnInsertar);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, btnAtras, 0, SpringLayout.WEST, lblNickname);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, btnAtras, -239, SpringLayout.WEST, btnInsertar);
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfApellido2.add(btnAtras);
		
		JLabel label = new JLabel("Apellido2");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblemail, 24, SpringLayout.SOUTH, label);
		label.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, label, 19, SpringLayout.SOUTH, lblApellido);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, label, 42, SpringLayout.WEST, tfApellido2);
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(label);
		
		txtApellido = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, txtApellido, -3, SpringLayout.NORTH, label);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, txtApellido, 89, SpringLayout.EAST, label);
		txtApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtApellido.setColumns(10);
		tfApellido2.add(txtApellido);
		
		JLabel lblPassword = new JLabel("Password");
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfNombre, -3, SpringLayout.NORTH, lblPassword);
		lblPassword.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, lblPassword, 42, SpringLayout.WEST, tfApellido2);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblNombre, 21, SpringLayout.SOUTH, lblPassword);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblPassword, 21, SpringLayout.SOUTH, lblNickname);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblPassword);
		
		passwordField = new JPasswordField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, passwordField, 115, SpringLayout.NORTH, lblNickname);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, passwordField, 0, SpringLayout.WEST, tfNick);
		sl_tfApellido2.putConstraint(SpringLayout.SOUTH, passwordField, 0, SpringLayout.SOUTH, lblApellido);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, passwordField, -436, SpringLayout.EAST, tfApellido2);
		tfApellido2.add(passwordField);
		
		JLabel lblDNI = new JLabel("DNI");
		sl_tfApellido2.putConstraint(SpringLayout.WEST, btnInsertar, -7, SpringLayout.WEST, lblDNI);
		lblDNI.setForeground(Color.WHITE);
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, lblDNI, 0, SpringLayout.NORTH, lblNombre);
		sl_tfApellido2.putConstraint(SpringLayout.EAST, lblDNI, -360, SpringLayout.EAST, tfApellido2);
		lblDNI.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfApellido2.add(lblDNI);
		
		tfDNI = new JTextField();
		sl_tfApellido2.putConstraint(SpringLayout.NORTH, tfDNI, -3, SpringLayout.NORTH, lblNombre);
		sl_tfApellido2.putConstraint(SpringLayout.WEST, tfDNI, 6, SpringLayout.EAST, lblDNI);
		tfDNI.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tfDNI.setColumns(10);
		tfApellido2.add(tfDNI);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Usuario.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		tfApellido2.add(lblNewLabel);
		//hasdasd
		
		btnInsertar.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				SQLUsuarios db = new SQLUsuarios();
		        try {
					db.SQLConnection("oracle", "root", "");
					
					
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       db.insertDataUsuario( tfDNI.getText(), tfNombre.getText(), tfApellido.getText(), txtApellido.getText(), tfNick.getText(), passwordField.getText(), tfemail.getText(), tftelefono.getText(), tffechaNacimiento.getText(), tfPreferencia.getText());
		       db.insertDataBIBLIO("seq_Biblioteca.currval", 100, "seq_Usuario.currval");        
		                
		               
		        db.closeConnection();
			}
		});
		
		
		
		
		
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
}
