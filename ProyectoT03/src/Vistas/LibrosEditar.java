package Vistas;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class LibrosEditar extends JFrame {
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;
	private JTextField textFieldISBN;
	private JTextField textFieldTitulo;
	private JTextField textFieldAutor;
	private JTextField textFieldAno;
	private JTextField textFieldEditorial;
	private JTextField textFieldGenero;
	private JTextField textFieldIdioma;
	private JTextField textFieldFormato;
	private JTextField textFieldResumen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LibrosEditar frame = new LibrosEditar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LibrosEditar() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Libros Editar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 675);
		getContentPane().setLayout(null);
		
		JLabel lblCdigoDelLibro = new JLabel("C\u00F3digo del libro");
		lblCdigoDelLibro.setForeground(Color.WHITE);
		lblCdigoDelLibro.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCdigoDelLibro.setBounds(40, 75, 127, 20);
		getContentPane().add(lblCdigoDelLibro);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setForeground(Color.WHITE);
		lblIsbn.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblIsbn.setBounds(40, 115, 116, 20);
		getContentPane().add(lblIsbn);
		
		JLabel lblNewLabel = new JLabel("T\u00EDtulo");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(40, 155, 116, 20);
		getContentPane().add(lblNewLabel);
		
		JLabel lblAo = new JLabel("Autor");
		lblAo.setForeground(Color.WHITE);
		lblAo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAo.setBounds(40, 195, 127, 20);
		getContentPane().add(lblAo);
		
		JLabel lblNewLabel_1 = new JLabel("A\u00F1o");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 235, 116, 20);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblFormato = new JLabel("G\u00E9nero");
		lblFormato.setForeground(Color.WHITE);
		lblFormato.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblFormato.setBounds(40, 315, 116, 20);
		getContentPane().add(lblFormato);
		
		JLabel lblResumen = new JLabel("Idioma");
		lblResumen.setForeground(Color.WHITE);
		lblResumen.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblResumen.setBounds(40, 355, 127, 20);
		getContentPane().add(lblResumen);
		
		JButton btnInsertar = new JButton("Editar");
		btnInsertar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnInsertar.setBounds(445, 597, 89, 23);
		getContentPane().add(btnInsertar);
		
		JButton btnAtrs = new JButton("Atr\u00E1s");
		btnAtrs.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtrs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtrs.setBounds(40, 597, 89, 23);
		getContentPane().add(btnAtrs);
		
		JLabel lblNewLabel_2 = new JLabel("Editorial");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 275, 116, 20);
		getContentPane().add(lblNewLabel_2);
		
		textFieldISBN = new JTextField();
		textFieldISBN.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldISBN.setColumns(10);
		textFieldISBN.setBounds(200, 110, 136, 25);
		getContentPane().add(textFieldISBN);
		
		textFieldTitulo = new JTextField();
		textFieldTitulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldTitulo.setColumns(10);
		textFieldTitulo.setBounds(200, 150, 136, 25);
		getContentPane().add(textFieldTitulo);
		
		textFieldAutor = new JTextField();
		textFieldAutor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldAutor.setColumns(10);
		textFieldAutor.setBounds(200, 190, 136, 25);
		getContentPane().add(textFieldAutor);
		
		textFieldAno = new JTextField();
		textFieldAno.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldAno.setColumns(10);
		textFieldAno.setBounds(200, 230, 136, 25);
		getContentPane().add(textFieldAno);
		
		textFieldEditorial = new JTextField();
		textFieldEditorial.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldEditorial.setColumns(10);
		textFieldEditorial.setBounds(200, 270, 136, 25);
		getContentPane().add(textFieldEditorial);
		
		textFieldGenero = new JTextField();
		textFieldGenero.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldGenero.setColumns(10);
		textFieldGenero.setBounds(200, 310, 136, 25);
		getContentPane().add(textFieldGenero);
		
		textFieldIdioma = new JTextField();
		textFieldIdioma.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldIdioma.setColumns(10);
		textFieldIdioma.setBounds(200, 350, 136, 25);
		getContentPane().add(textFieldIdioma);
		
		final JComboBox<Object> comboBox = new JComboBox();

		comboBox.setBounds(200, 77, 136, 20);
		getContentPane().add(comboBox);
		
		SQLLibros db = new SQLLibros();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryLibros = "SELECT CODIGO FROM LIBRO ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryLibros);
            ResultSet rs = st.executeQuery(QueryLibros);
            while(rs.next()){
            	
            	comboBox.addItem(rs.getObject("CODIGO"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
		            String QueryAutor = "SELECT * FROM libro WHERE CODIGO = "+comboBox.getSelectedItem();
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryAutor);
				
				
		            while (resultSet.next()) {
		            	
		            	
		            	textFieldISBN.setText(resultSet.getString("ISBN"));
		            	textFieldTitulo.setText(resultSet.getString("Titulo"));
		            	textFieldAutor.setText(resultSet.getString("Autor"));
		            	textFieldAno.setText(resultSet.getString("A�o"));
		            	textFieldEditorial.setText(resultSet.getString("Editorial"));
		            	textFieldGenero.setText(resultSet.getString("Genero"));
		            	textFieldIdioma.setText(resultSet.getString("Idioma"));
		            	textFieldFormato.setText(resultSet.getString("Formato"));
		            	textFieldResumen.setText(resultSet.getString("Resumen"));
		              
		            }
				
				

				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				
				
			}	
			});
				
			
		
		
		
		
		JButton btnNewButton = new JButton("Borrar");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLLibros db = new SQLLibros();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.deleteRecord(comboBox.getSelectedItem());
		
				
			
			}
		});
		btnNewButton.setBounds(346, 597, 89, 23);
		getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("Formato");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 400, 127, 22);
		getContentPane().add(lblNewLabel_3);
		
		textFieldFormato = new JTextField();
		textFieldFormato.setBounds(200, 397, 136, 25);
		getContentPane().add(textFieldFormato);
		textFieldFormato.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Resumen");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 440, 127, 20);
		getContentPane().add(lblNewLabel_4);
		
		textFieldResumen = new JTextField();
		textFieldResumen.setBounds(200, 437, 334, 131);
		getContentPane().add(textFieldResumen);
		textFieldResumen.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("New label");
		lblNewLabel_5.setIcon(new ImageIcon(LibrosEditar.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_5.setBounds(0, 0, 784, 637);
		getContentPane().add(lblNewLabel_5);
		
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLLibros db = new SQLLibros();
				try {
					db.SQLConnection("oracle", "root", "");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.alterRecord(comboBox.getSelectedItem(), textFieldISBN.getText(), textFieldTitulo.getText(), textFieldAutor.getText(), textFieldAno.getText(), textFieldEditorial.getText(), textFieldGenero.getText(), textFieldIdioma.getText(), textFieldFormato.getText(), textFieldResumen.getText());
			
			}
		});
	}
}
