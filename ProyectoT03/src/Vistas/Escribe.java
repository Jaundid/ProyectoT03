package Vistas;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JList;
import java.awt.Color;
import javax.swing.ImageIcon;

public class Escribe extends JFrame {
	int i=0;
	private DefaultListModel<String> mLista = new DefaultListModel();
	private JPanel contentPane;
	private JPanel contentPane_1;
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Escribe frame = new Escribe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Escribe() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Escribe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 416, 314);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		contentPane_1 = new JPanel();
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane_1);
		contentPane_1.setLayout(null);
		
		JLabel lblAutor = new JLabel("Autor");
		lblAutor.setForeground(Color.WHITE);
		lblAutor.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAutor.setBounds(50, 22, 81, 19);
		contentPane_1.add(lblAutor);
		
		
		
		
		JButton btnAtrs = new JButton("Atr\u00E1s");
		btnAtrs.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtrs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtrs.setBounds(173, 241, 89, 23);
		contentPane_1.add(btnAtrs);
		
		final JList list = new JList(mLista);
		list.setBounds(132, 54, 144, 153);
		contentPane_1.add(list);
		
		JLabel lblListaDeAutores = new JLabel("Lista Libros");
		lblListaDeAutores.setForeground(Color.WHITE);
		lblListaDeAutores.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblListaDeAutores.setBounds(10, 76, 112, 17);
		contentPane_1.add(lblListaDeAutores);
		
		final JComboBox<Object> comboBoxAutor = new JComboBox();
		comboBoxAutor.setBounds(154, 23, 97, 20);
		contentPane_1.add(comboBoxAutor);
		
		final JComboBox comboBoxLibro = new JComboBox();
		comboBoxLibro.setBounds(171, 76, 89, 20);
		//contentPane_1.add(comboBoxLibro);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Escribe.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel.setBounds(0, 0, 400, 276);
		contentPane_1.add(lblNewLabel);
		SQLAutor db = new SQLAutor();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryAutor = "SELECT NOMBRE FROM AUTOR ORDER BY CODIGO ASC";
			Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement st = Conexion.createStatement();
            st.executeQuery(QueryAutor);
            ResultSet rs = st.executeQuery(QueryAutor);
            while(rs.next()){
            	
            	comboBoxAutor.addItem(rs.getObject("NOMBRE"));
            }
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		comboBoxAutor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					mLista.removeAllElements();
		            String QueryAutor = "select codigo from autor where nombre="+"'"+comboBoxAutor.getSelectedItem()+"'"; //aqui pillar� el codigo seg�n el nombre seleccionado del combobox
		            
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            Statement rt=Conexion.createStatement();
		            
		            ResultSet rA= st.executeQuery(QueryAutor);
		            
				
				
		            while (rA.next()) {
		            i=rA.getInt("CODIGO"); //aqu� lo almacenar� en la variable
		             //list.setModel(mLista);
		            
		            }
		            String QA="select titulo from libro where codigo="+i; //seleccionara el titulo segun el codigo del autor
		            ResultSet RL= rt.executeQuery(QA);

		            while(RL.next()){
		            	mLista.addElement(RL.getString("TITULO")); //mostrara el titulo, si a�ades un libro a un autor, teniendo 3 autores, y estos 2 restantes no tienen libros, ver�s que solo saldr� el de 1 autor
		            }
		            
				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				
			}
		});
		
		
	}
}
