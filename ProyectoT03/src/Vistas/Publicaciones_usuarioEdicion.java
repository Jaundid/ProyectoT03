package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Publicaciones_usuarioEdicion extends JFrame {
	static String dbname="";
	static String user="oracle";
	static String pass="root";
	private static Connection Conexion=null;
	private JPanel contentPane;
	private JTextField tfFecha;
	private JTextField tfArchivos;
	private JTextField tfIdioma;
	private JTextField tfFormato;
	int i=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Publicaciones_usuarioEdicion frame = new Publicaciones_usuarioEdicion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Publicaciones_usuarioEdicion() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Menu.class.getResource("/Documentos/Icono.png")));
		setTitle("Publicaciones Usuarios Editar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 517, 523);
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 517, 523);
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Fecha Publicaci\u00F3n");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(40, 108, 136, 19);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Archivos");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 149, 91, 19);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("C\u00F3digo Archivo");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 63, 136, 19);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Autor");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 190, 79, 19);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("G\u00E9nero");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 230, 110, 19);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Idioma");
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_5.setBounds(40, 270, 91, 19);
		contentPane.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Formato");
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_6.setBounds(40, 310, 96, 19);
		contentPane.add(lblNewLabel_6);
		
		tfFecha = new JTextField();
		tfFecha.setEditable(false);
		tfFecha.setBounds(200, 101, 136, 25);
		contentPane.add(tfFecha);
		tfFecha.setColumns(10);
		
		tfArchivos = new JTextField();
		tfArchivos.setBounds(200, 148, 136, 25);
		contentPane.add(tfArchivos);
		tfArchivos.setColumns(10);
		
		tfIdioma = new JTextField();
		tfIdioma.setBounds(200, 270, 136, 25);
		contentPane.add(tfIdioma);
		tfIdioma.setColumns(10);
		
		tfFormato = new JTextField();
		tfFormato.setBounds(200, 310, 136, 25);
		contentPane.add(tfFormato);
		tfFormato.setColumns(10);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnEditar.setBounds(200, 430, 89, 23);
		contentPane.add(btnEditar);
		
		JButton btnAtras = new JButton("Atr\u00E1s");
		btnAtras.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAtras.setBounds(40, 430, 89, 23);
		contentPane.add(btnAtras);
		
		final JComboBox comboBoxCodigo = new JComboBox();
		
		comboBoxCodigo.setBounds(200, 64, 136, 20);
		contentPane.add(comboBoxCodigo);
	
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnBorrar.setBounds(299, 430, 89, 23);
		contentPane.add(btnBorrar);
		
		final JComboBox comboBoxAutor = new JComboBox();
		comboBoxAutor.setBounds(198, 191, 91, 20);
		contentPane.add(comboBoxAutor);
		
		final JComboBox comboBoxGenero = new JComboBox();
		comboBoxGenero.setBounds(198, 231, 91, 20);
		contentPane.add(comboBoxGenero);
		
		JLabel lblNewLabel_7 = new JLabel("New label");
		lblNewLabel_7.setIcon(new ImageIcon(Publicaciones_usuarioEdicion.class.getResource("/Documentos/NB-Dp4Apps-background.jpg")));
		lblNewLabel_7.setBounds(0, 0, 784, 601);
		contentPane.add(lblNewLabel_7);
		
		
		SQLpublicaci�nUsuario db = new SQLpublicaci�nUsuario();
		try {
			db.SQLConnection("oracle", "root", "");
			String QueryPublicaciones_usuario = "SELECT CODIGOPUBLICACION FROM PUBLICACION_USUARIO ORDER BY CODIGOPUBLICACION ";
			String QueryAutor = "SELECT NOMBRE FROM USUARIO";
			String QueryGenero="SELECT TIPO FROM GENERO ORDER BY TIPO";
		    Conexion= DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
			Statement stU = ((Connection) Conexion).createStatement();
			Statement stA = ((Connection) Conexion).createStatement();
			Statement stG = ((Connection) Conexion).createStatement();
            stU.executeQuery(QueryPublicaciones_usuario);
            stA.executeQuery(QueryAutor);
            stG.executeQuery(QueryGenero);
            ResultSet rsU = stU.executeQuery(QueryPublicaciones_usuario);
            ResultSet rsA = stA.executeQuery(QueryAutor);
            ResultSet rsG = stG.executeQuery(QueryGenero);
            while(rsU.next()){
            	
            	comboBoxCodigo.addItem(rsU.getObject("CODIGOPUBLICACION"));
            }
            while(rsA.next()){
            	
            	comboBoxAutor.addItem(rsA.getObject("NOMBRE"));
            }
            while(rsG.next()){
            	
            	comboBoxGenero.addItem(rsG.getObject("TIPO"));
            }
            db.closeConnection();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLpublicaci�nUsuario db = new SQLpublicaci�nUsuario();
				db.deleteRecordPubiUsuario(comboBoxCodigo.getSelectedItem());
				JOptionPane.showMessageDialog(null, "Se ha borrado el registro");
			}
		});
		
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SQLpublicaci�nUsuario db = new SQLpublicaci�nUsuario();
				try {
					db.SQLConnection("oracle", "root", "");
					String QueryAutor = "SELECT CODIGO FROM USUARIO WHERE NOMBRE="+"'"+comboBoxAutor.getSelectedItem()+"'";
					
					Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
					
		         
		            Statement stA = Conexion.createStatement();
					
					
		            stA.executeQuery(QueryAutor);
		           
		            
		            ResultSet rsA = stA.executeQuery(QueryAutor);
		         
		            
		            while(rsA.next()){
		            	i=rsA.getInt("CODIGO");
		            }
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				db.alterRecordPubli(comboBoxCodigo.getSelectedItem(), tfArchivos.getText(), i, comboBoxGenero.getSelectedItem(), tfIdioma.getText(), tfFormato.getText());
				
			}
		});
		
		comboBoxCodigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
		            String QueryAutor = "SELECT * FROM PUBLICACION_USUARIO WHERE CODIGOPUBLICACION = "+comboBoxCodigo.getSelectedItem();
		            Conexion=DriverManager.getConnection("jdbc:oracle:thin:@localhost" + dbname, user, pass);
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(QueryAutor);
				
				
		            while (resultSet.next()) {
		            tfArchivos.setText(resultSet.getString("NOMBREARCHIVO"));
		            tfFecha.setText(resultSet.getString("FECHAPUBLICACION"));
		            tfFormato.setText(resultSet.getString("FORMATO"));
		            tfIdioma.setText(resultSet.getString("IDIOMA"));
			}
				} catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisición de datos");
		        } 
				}
		});
		
	}
}
